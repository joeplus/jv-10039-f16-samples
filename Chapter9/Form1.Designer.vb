﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.mtbPhone = New System.Windows.Forms.MaskedTextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.lstContacts = New System.Windows.Forms.ListBox()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnLoadContacts = New System.Windows.Forms.Button()
        Me.btnSaveContacts = New System.Windows.Forms.Button()
        Me.ofdContacts = New System.Windows.Forms.OpenFileDialog()
        Me.sfdContacts = New System.Windows.Forms.SaveFileDialog()
        Me.btnSaveAs = New System.Windows.Forms.Button()
        Me.btnOpenDlg = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(83, 39)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Username:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(40, 105)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(145, 24)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Phone Number:"
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(194, 36)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(143, 29)
        Me.txtUsername.TabIndex = 2
        '
        'mtbPhone
        '
        Me.mtbPhone.Location = New System.Drawing.Point(194, 102)
        Me.mtbPhone.Mask = "(999) 000-0000"
        Me.mtbPhone.Name = "mtbPhone"
        Me.mtbPhone.Size = New System.Drawing.Size(143, 29)
        Me.mtbPhone.TabIndex = 3
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(44, 178)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(100, 40)
        Me.btnSave.TabIndex = 4
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnLoad
        '
        Me.btnLoad.Location = New System.Drawing.Point(194, 178)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(100, 40)
        Me.btnLoad.TabIndex = 5
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'lstContacts
        '
        Me.lstContacts.FormattingEnabled = True
        Me.lstContacts.ItemHeight = 24
        Me.lstContacts.Location = New System.Drawing.Point(511, 36)
        Me.lstContacts.Name = "lstContacts"
        Me.lstContacts.Size = New System.Drawing.Size(240, 148)
        Me.lstContacts.TabIndex = 6
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(345, 178)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(90, 40)
        Me.btnAdd.TabIndex = 7
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnLoadContacts
        '
        Me.btnLoadContacts.Location = New System.Drawing.Point(781, 105)
        Me.btnLoadContacts.Name = "btnLoadContacts"
        Me.btnLoadContacts.Size = New System.Drawing.Size(100, 40)
        Me.btnLoadContacts.TabIndex = 9
        Me.btnLoadContacts.Text = "Load"
        Me.btnLoadContacts.UseVisualStyleBackColor = True
        '
        'btnSaveContacts
        '
        Me.btnSaveContacts.Location = New System.Drawing.Point(781, 36)
        Me.btnSaveContacts.Name = "btnSaveContacts"
        Me.btnSaveContacts.Size = New System.Drawing.Size(100, 40)
        Me.btnSaveContacts.TabIndex = 8
        Me.btnSaveContacts.Text = "Save"
        Me.btnSaveContacts.UseVisualStyleBackColor = True
        '
        'ofdContacts
        '
        Me.ofdContacts.FileName = "contacts.txt"
        '
        'sfdContacts
        '
        Me.sfdContacts.FileName = "contacts.txt"
        '
        'btnSaveAs
        '
        Me.btnSaveAs.Location = New System.Drawing.Point(781, 178)
        Me.btnSaveAs.Name = "btnSaveAs"
        Me.btnSaveAs.Size = New System.Drawing.Size(100, 40)
        Me.btnSaveAs.TabIndex = 10
        Me.btnSaveAs.Text = "Save As"
        Me.btnSaveAs.UseVisualStyleBackColor = True
        '
        'btnOpenDlg
        '
        Me.btnOpenDlg.Location = New System.Drawing.Point(781, 253)
        Me.btnOpenDlg.Name = "btnOpenDlg"
        Me.btnOpenDlg.Size = New System.Drawing.Size(100, 40)
        Me.btnOpenDlg.TabIndex = 11
        Me.btnOpenDlg.Text = "Open"
        Me.btnOpenDlg.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 24.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1008, 705)
        Me.Controls.Add(Me.btnOpenDlg)
        Me.Controls.Add(Me.btnSaveAs)
        Me.Controls.Add(Me.btnLoadContacts)
        Me.Controls.Add(Me.btnSaveContacts)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.lstContacts)
        Me.Controls.Add(Me.btnLoad)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.mtbPhone)
        Me.Controls.Add(Me.txtUsername)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtUsername As TextBox
    Friend WithEvents mtbPhone As MaskedTextBox
    Friend WithEvents btnSave As Button
    Friend WithEvents btnLoad As Button
    Friend WithEvents lstContacts As ListBox
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnLoadContacts As Button
    Friend WithEvents btnSaveContacts As Button
    Friend WithEvents ofdContacts As OpenFileDialog
    Friend WithEvents sfdContacts As SaveFileDialog
    Friend WithEvents btnSaveAs As Button
    Friend WithEvents btnOpenDlg As Button
End Class
