﻿Imports System.IO

Public Structure User
    Public strName As String
    Public strPhone As String

    Public Sub New(strUsername As String, strUserPhone As String)
        strName = strUsername
        strPhone = strUserPhone
    End Sub
End Structure

Public Class Form1
    Private strFilename As String
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim swrContact As StreamWriter = File.AppendText("contact.txt")
            swrContact.WriteLine(txtUsername.Text)
            swrContact.WriteLine(mtbPhone.Text)
            swrContact.Close()
        Catch Ex As Exception
            MessageBox.Show("Failure saving contact due to exception: " + Ex.Message)
        End Try

        txtUsername.ResetText()
        mtbPhone.ResetText()
    End Sub

    Private Sub btnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Try
            Dim srdContact As StreamReader = File.OpenText("contact.txt")
            txtUsername.Text = srdContact.ReadLine()
            mtbPhone.Text = srdContact.ReadLine()
            srdContact.Close()
        Catch Ex As Exception
            MessageBox.Show("Failure saving contact due to exception: " + Ex.Message)
        End Try

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        lstContacts.Items.Add(txtUsername.Text + ", " + mtbPhone.Text)
    End Sub

    Private Sub btnSaveContacts_Click(sender As Object, e As EventArgs) Handles btnSaveContacts.Click
        Try
            Dim swrContact As StreamWriter = File.CreateText(strFilename)
            For Each strContact As String In lstContacts.Items
                swrContact.WriteLine(strContact)
            Next
            swrContact.Close()
        Catch Ex As Exception
            MessageBox.Show("Failure saving contact list due to exception: " + Ex.Message)
        End Try
    End Sub

    Private Sub btnLoadContacts_Click(sender As Object, e As EventArgs) Handles btnLoadContacts.Click
        lstContacts.Items.Clear()

        Try
            Dim srdContact As StreamReader = File.OpenText(strFilename)
            While Not srdContact.EndOfStream
                Dim strContact As String = srdContact.ReadLine()
                lstContacts.Items.Add(strContact)
            End While
            srdContact.Close()
        Catch Ex As Exception
            MessageBox.Show("Failure saving contact list due to exception: " + Ex.Message)
        End Try
    End Sub

    Private Sub btnOpenDlg_Click(sender As Object, e As EventArgs) Handles btnOpenDlg.Click
        If ofdContacts.ShowDialog() = DialogResult.OK Then
            strFilename = ofdContacts.FileName
            btnLoadContacts_Click(sender, e)
        End If
    End Sub

    Private Sub btnSaveAs_Click(sender As Object, e As EventArgs) Handles btnSaveAs.Click
        If sfdContacts.ShowDialog() = DialogResult.OK Then
            strFilename = sfdContacts.FileName
            btnSaveContacts_Click(sender, e)
        End If
    End Sub

    Private Sub lstContacts_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstContacts.SelectedIndexChanged
        Dim strSelected As String = lstContacts.SelectedItem.ToString()
        Dim strFields As String() = strSelected.Split(New Char() {","}, 2)
        Dim usrSelected As New User(strFields(0).Trim(), strFields(1).Trim())
    End Sub
End Class
