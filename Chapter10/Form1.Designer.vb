﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstName = New System.Windows.Forms.ListBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lstCustomers = New System.Windows.Forms.ListBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lsvCustomers = New System.Windows.Forms.ListView()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lsvStuff = New System.Windows.Forms.ListView()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.btnUpdate = New System.Windows.Forms.Button()
        Me.btnRead = New System.Windows.Forms.Button()
        Me.btnCreate = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.txtRegionID = New System.Windows.Forms.TextBox()
        Me.txtTerrDesc = New System.Windows.Forms.TextBox()
        Me.txtTerrID = New System.Windows.Forms.TextBox()
        Me.tbName = New System.Windows.Forms.TextBox()
        Me.tbCity = New System.Windows.Forms.TextBox()
        Me.tbCountry = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.tbStuff = New System.Windows.Forms.TextBox()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.SuspendLayout()
        '
        'lstName
        '
        Me.lstName.FormattingEnabled = True
        Me.lstName.ItemHeight = 24
        Me.lstName.Location = New System.Drawing.Point(11, 11)
        Me.lstName.Margin = New System.Windows.Forms.Padding(6)
        Me.lstName.Name = "lstName"
        Me.lstName.Size = New System.Drawing.Size(217, 172)
        Me.lstName.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Location = New System.Drawing.Point(22, -2)
        Me.TabControl1.Margin = New System.Windows.Forms.Padding(6)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(829, 493)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.lstName)
        Me.TabPage1.Location = New System.Drawing.Point(4, 33)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage1.Size = New System.Drawing.Size(821, 456)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Employees ListBox"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lstCustomers)
        Me.TabPage2.Location = New System.Drawing.Point(4, 33)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage2.Size = New System.Drawing.Size(821, 456)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Customers ListBox"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lstCustomers
        '
        Me.lstCustomers.FormattingEnabled = True
        Me.lstCustomers.ItemHeight = 24
        Me.lstCustomers.Location = New System.Drawing.Point(11, 11)
        Me.lstCustomers.Margin = New System.Windows.Forms.Padding(6)
        Me.lstCustomers.Name = "lstCustomers"
        Me.lstCustomers.Size = New System.Drawing.Size(715, 388)
        Me.lstCustomers.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lsvCustomers)
        Me.TabPage3.Location = New System.Drawing.Point(4, 33)
        Me.TabPage3.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage3.Size = New System.Drawing.Size(821, 456)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Customers ListView"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lsvCustomers
        '
        Me.lsvCustomers.Location = New System.Drawing.Point(11, 11)
        Me.lsvCustomers.Margin = New System.Windows.Forms.Padding(6)
        Me.lsvCustomers.Name = "lsvCustomers"
        Me.lsvCustomers.Size = New System.Drawing.Size(765, 405)
        Me.lsvCustomers.TabIndex = 0
        Me.lsvCustomers.UseCompatibleStateImageBehavior = False
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.lsvStuff)
        Me.TabPage4.Location = New System.Drawing.Point(4, 33)
        Me.TabPage4.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(6)
        Me.TabPage4.Size = New System.Drawing.Size(821, 456)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Stuff ListView"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lsvStuff
        '
        Me.lsvStuff.Location = New System.Drawing.Point(11, 11)
        Me.lsvStuff.Margin = New System.Windows.Forms.Padding(6)
        Me.lsvStuff.Name = "lsvStuff"
        Me.lsvStuff.Size = New System.Drawing.Size(770, 419)
        Me.lsvStuff.TabIndex = 0
        Me.lsvStuff.UseCompatibleStateImageBehavior = False
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.btnClear)
        Me.TabPage5.Controls.Add(Me.btnDelete)
        Me.TabPage5.Controls.Add(Me.btnUpdate)
        Me.TabPage5.Controls.Add(Me.btnRead)
        Me.TabPage5.Controls.Add(Me.btnCreate)
        Me.TabPage5.Controls.Add(Me.Label6)
        Me.TabPage5.Controls.Add(Me.Label5)
        Me.TabPage5.Controls.Add(Me.Label4)
        Me.TabPage5.Controls.Add(Me.lblStatus)
        Me.TabPage5.Controls.Add(Me.txtRegionID)
        Me.TabPage5.Controls.Add(Me.txtTerrDesc)
        Me.TabPage5.Controls.Add(Me.txtTerrID)
        Me.TabPage5.Location = New System.Drawing.Point(4, 33)
        Me.TabPage5.Margin = New System.Windows.Forms.Padding(6)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(821, 456)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "CRUD"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(637, 248)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(88, 37)
        Me.btnClear.TabIndex = 11
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(494, 248)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(109, 37)
        Me.btnDelete.TabIndex = 10
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnUpdate
        '
        Me.btnUpdate.Location = New System.Drawing.Point(365, 248)
        Me.btnUpdate.Name = "btnUpdate"
        Me.btnUpdate.Size = New System.Drawing.Size(100, 37)
        Me.btnUpdate.TabIndex = 9
        Me.btnUpdate.Text = "Update"
        Me.btnUpdate.UseVisualStyleBackColor = True
        '
        'btnRead
        '
        Me.btnRead.Location = New System.Drawing.Point(226, 248)
        Me.btnRead.Name = "btnRead"
        Me.btnRead.Size = New System.Drawing.Size(114, 37)
        Me.btnRead.TabIndex = 8
        Me.btnRead.Text = "Read"
        Me.btnRead.UseVisualStyleBackColor = True
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(54, 248)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(137, 37)
        Me.btnCreate.TabIndex = 7
        Me.btnCreate.Text = "Create/Insert"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(50, 175)
        Me.Label6.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(98, 24)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Region ID:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 105)
        Me.Label5.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(154, 24)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Terr. Description:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 30)
        Me.Label4.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 24)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Terr. ID:"
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Location = New System.Drawing.Point(53, 412)
        Me.lblStatus.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(66, 24)
        Me.lblStatus.TabIndex = 3
        Me.lblStatus.Text = "Label4"
        '
        'txtRegionID
        '
        Me.txtRegionID.Location = New System.Drawing.Point(209, 170)
        Me.txtRegionID.Margin = New System.Windows.Forms.Padding(6)
        Me.txtRegionID.Name = "txtRegionID"
        Me.txtRegionID.Size = New System.Drawing.Size(180, 29)
        Me.txtRegionID.TabIndex = 2
        '
        'txtTerrDesc
        '
        Me.txtTerrDesc.Location = New System.Drawing.Point(209, 100)
        Me.txtTerrDesc.Margin = New System.Windows.Forms.Padding(6)
        Me.txtTerrDesc.Name = "txtTerrDesc"
        Me.txtTerrDesc.Size = New System.Drawing.Size(180, 29)
        Me.txtTerrDesc.TabIndex = 1
        '
        'txtTerrID
        '
        Me.txtTerrID.Location = New System.Drawing.Point(209, 30)
        Me.txtTerrID.Margin = New System.Windows.Forms.Padding(6)
        Me.txtTerrID.Name = "txtTerrID"
        Me.txtTerrID.Size = New System.Drawing.Size(180, 29)
        Me.txtTerrID.TabIndex = 0
        '
        'tbName
        '
        Me.tbName.Location = New System.Drawing.Point(176, 504)
        Me.tbName.Margin = New System.Windows.Forms.Padding(6)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(180, 29)
        Me.tbName.TabIndex = 2
        '
        'tbCity
        '
        Me.tbCity.Location = New System.Drawing.Point(176, 552)
        Me.tbCity.Margin = New System.Windows.Forms.Padding(6)
        Me.tbCity.Name = "tbCity"
        Me.tbCity.Size = New System.Drawing.Size(180, 29)
        Me.tbCity.TabIndex = 3
        '
        'tbCountry
        '
        Me.tbCountry.Location = New System.Drawing.Point(176, 600)
        Me.tbCountry.Margin = New System.Windows.Forms.Padding(6)
        Me.tbCountry.Name = "tbCountry"
        Me.tbCountry.Size = New System.Drawing.Size(180, 29)
        Me.tbCountry.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(79, 508)
        Me.Label1.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 24)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Name:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(79, 558)
        Me.Label2.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 24)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "City:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(79, 606)
        Me.Label3.Margin = New System.Windows.Forms.Padding(6, 0, 6, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 24)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Country:"
        '
        'tbStuff
        '
        Me.tbStuff.Location = New System.Drawing.Point(418, 502)
        Me.tbStuff.Margin = New System.Windows.Forms.Padding(6)
        Me.tbStuff.Name = "tbStuff"
        Me.tbStuff.Size = New System.Drawing.Size(481, 29)
        Me.tbStuff.TabIndex = 8
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(501, 545)
        Me.btnRemove.Margin = New System.Windows.Forms.Padding(6)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(138, 42)
        Me.btnRemove.TabIndex = 9
        Me.btnRemove.Text = "Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 24.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(924, 668)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.tbStuff)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tbCountry)
        Me.Controls.Add(Me.tbCity)
        Me.Controls.Add(Me.tbName)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstName As ListBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents lstCustomers As ListBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents lsvCustomers As ListView
    Friend WithEvents tbName As TextBox
    Friend WithEvents tbCity As TextBox
    Friend WithEvents tbCountry As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents lsvStuff As ListView
    Friend WithEvents tbStuff As TextBox
    Friend WithEvents btnRemove As Button
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents btnClear As Button
    Friend WithEvents btnDelete As Button
    Friend WithEvents btnUpdate As Button
    Friend WithEvents btnRead As Button
    Friend WithEvents btnCreate As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblStatus As Label
    Friend WithEvents txtRegionID As TextBox
    Friend WithEvents txtTerrDesc As TextBox
    Friend WithEvents txtTerrID As TextBox
End Class
