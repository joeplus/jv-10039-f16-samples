﻿Imports System.Data.SqlClient

Public Class Form1
    Dim strConnection As String = "Data Source=.\SQLEXPRESS;Initial Catalog=Northwind;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadEmployeesListBox()
        LoadCustomersListBox()

        SetupCustomersListView()
        LoadCustomersListView()

        SetupStuffListView()
        LoadStuffListView()
    End Sub

    Private Sub LoadEmployeesListBox()
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand("Select * From Employees", myConnection)
            Dim myDR As SqlDataReader = myCmd.ExecuteReader()
            While myDR.Read()
                lstName.Items.Add(myDR("LastName"))
            End While
            myDR.Close()
        Catch ex As Exception
            MessageBox.Show("Failed to select employees from Northwind DB due to: " + ex.GetBaseException.ToString(), "COMP10039 DB")
        Finally
            myConnection.Close()
        End Try
    End Sub

    Private Sub LoadCustomersListBox()
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand("Select CompanyName, City, Country From Customers", myConnection)
            Dim myDR As SqlDataReader = myCmd.ExecuteReader()
            While myDR.Read()
                lstCustomers.Items.Add(myDR("CompanyName") + "," + myDR("City") + "," + myDR("Country"))
            End While
            myDR.Close()
        Catch ex As Exception
            MessageBox.Show("Failed to select customers from Northwind DB due to: " + ex.GetBaseException.ToString(), "COMP10039 DB")
        Finally
            myConnection.Close()
        End Try
    End Sub

    Private Sub SetupCustomersListView()
        lsvCustomers.View = View.Details
        lsvCustomers.MultiSelect = False
        lsvCustomers.GridLines = True
        lsvCustomers.FullRowSelect = True

        lsvCustomers.Columns.Add("Row", 50, HorizontalAlignment.Left)
        lsvCustomers.Columns.Add("Company", -1, HorizontalAlignment.Left)
        lsvCustomers.Columns.Add("City", -1, HorizontalAlignment.Left)
        lsvCustomers.Columns.Add("Country", -1, HorizontalAlignment.Left)
    End Sub
    '
    Private Sub LoadCustomersListView()
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand("Select CompanyName, City, Country From Customers", myConnection)
            Dim myDR As SqlDataReader = myCmd.ExecuteReader()
            Dim intRow As Integer
            While myDR.Read()
                'lstCustomers.Items.Add(myDR("CompanyName") + "," + myDR("City") + "," + myDR("Country"))
                intRow += 1
                Dim lsiCustomer As ListViewItem = New ListViewItem(intRow)
                lsiCustomer.SubItems.Add(myDR("CompanyName"))
                lsiCustomer.SubItems.Add(myDR("City"))
                lsiCustomer.SubItems.Add(myDR("Country"))
                lsvCustomers.Items.Add(lsiCustomer)
            End While
            myDR.Close()
        Catch ex As Exception
            MessageBox.Show("Failed to select customers from Northwind DB due to: " + ex.GetBaseException.ToString(), "COMP10039 DB")
        Finally
            myConnection.Close()
        End Try
    End Sub

    Private Sub lstCustomers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstCustomers.SelectedIndexChanged
        If lstCustomers.SelectedItems.Count > 0 Then
            Dim strSelection As String = lstCustomers.SelectedItems(0).ToString()
            Dim strFields() As String = strSelection.Split(",")
            tbName.Text = strFields(0)
            tbCity.Text = strFields(1)
            tbCountry.Text = strFields(2)
        End If
    End Sub

    Private Sub lsvCustomers_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvCustomers.SelectedIndexChanged
        If lsvCustomers.SelectedItems.Count > 0 Then
            Dim lviSelected As ListViewItem = lsvCustomers.SelectedItems(0)
            tbName.Text = lviSelected.SubItems(1).Text
            tbCity.Text = lviSelected.SubItems(2).Text
            tbCountry.Text = lviSelected.SubItems(3).Text
        End If
    End Sub

    Private Sub SetupStuffListView()
        'configure visual properties
        lsvStuff.View = View.Details
        lsvStuff.MultiSelect = True
        lsvStuff.FullRowSelect = True
        lsvStuff.GridLines = True

        'define the columns
        lsvStuff.Columns.Add("Row", 50, HorizontalAlignment.Left)
        lsvStuff.Columns.Add("Fruit", 50, HorizontalAlignment.Left)
        lsvStuff.Columns.Add("Car", 50, HorizontalAlignment.Left)
        lsvStuff.Columns.Add("Person", 50, HorizontalAlignment.Left)
    End Sub

    Private Sub LoadStuffListView()
        Dim arrStuff(,) As String = {{"Apple", "Civic", "Aman"},
                                     {"Blueberry", "Bonneville", "Betty"},
                                     {"Pear", "Pilot", "Pam"}
                                    }

        For intCounter As Integer = 0 To arrStuff.GetLength(0) - 1
            Dim lviStuff As ListViewItem = New ListViewItem(intCounter)
            lviStuff.SubItems.Add(arrStuff(intCounter, 0))
            lviStuff.SubItems.Add(arrStuff(intCounter, 1))
            lviStuff.SubItems.Add(arrStuff(intCounter, 2))

            lsvStuff.Items.Add(lviStuff)
        Next
    End Sub

    Private Sub lsvStuff_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lsvStuff.SelectedIndexChanged
        tbStuff.ResetText()

        For intCount As Integer = 0 To lsvStuff.SelectedItems.Count - 1
            Dim lviSelection As ListViewItem = lsvStuff.SelectedItems(intCount)
            tbStuff.Text += lviSelection.SubItems(0).Text + "," +
                            lviSelection.SubItems(1).Text + "," +
                            lviSelection.SubItems(2).Text + "," +
                            lviSelection.SubItems(3).Text
        Next
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        For intCount As Integer = 0 To lsvStuff.SelectedItems.Count - 1
            lsvStuff.SelectedItems(0).Remove()
        Next
    End Sub

    Private Sub btnCreate_Click(sender As Object, e As EventArgs) Handles btnCreate.Click
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand()
            myCmd.Connection = myConnection
            myCmd.CommandText = "INSERT INTO Territories (TerritoryID, TerritoryDescription, RegionID)" +
                                   " VALUES (@TerrID, @TerrDesc, @Region)"

            myCmd.Parameters.Add("@TerrID", SqlDbType.NVarChar)
            myCmd.Parameters("@TerrID").Value = txtTerrID.Text

            myCmd.Parameters.Add("@TerrDesc", SqlDbType.NChar)
            myCmd.Parameters("@TerrDesc").Value = txtTerrDesc.Text

            myCmd.Parameters.Add("@Region", SqlDbType.Int)
            myCmd.Parameters("@Region").Value = Integer.Parse(txtRegionID.Text)

            Dim intRows As Integer = myCmd.ExecuteNonQuery()
            If intRows = 1 Then
                lblStatus.Text = "Insertion succeeded"
            Else
                lblStatus.Text = "Insertion failed"
            End If

        Catch ex As Exception
            MessageBox.Show("Insertion into Territories failed due to exception: " + ex.Message, "COMP10039 CH10")
        Finally
            myConnection.Close()
        End Try
    End Sub

    Private Sub btnRead_Click(sender As Object, e As EventArgs) Handles btnRead.Click
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand()
            myCmd.Connection = myConnection
            myCmd.CommandText = "SELECT * FROM Territories WHERE TerritoryID = @TerrID"

            myCmd.Parameters.Add("@TerrID", SqlDbType.NVarChar)
            myCmd.Parameters("@TerrID").Value = txtTerrID.Text

            Dim myRdr As SqlDataReader = myCmd.ExecuteReader()
            If myRdr.Read() Then
                txtRegionID.Text = myRdr("RegionID")
                txtTerrDesc.Text = myRdr("TerritoryDescription")
                lblStatus.Text = "Read succeeded"
            Else
                lblStatus.Text = "Read failed"
            End If

        Catch ex As Exception
            MessageBox.Show("Read from Territories failed due to exception: " + ex.Message,
                            "COMP10039 CH10")
        Finally
            myConnection.Close()
        End Try
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand()
            myCmd.Connection = myConnection
            myCmd.CommandText = "UPDATE Territories SET TerritoryDescription = @TerrDesc, RegionID = @Region" +
                                    " WHERE TerritoryID = @TerrID"

            myCmd.Parameters.Add("@TerrID", SqlDbType.NVarChar)
            myCmd.Parameters("@TerrID").Value = txtTerrID.Text

            myCmd.Parameters.Add("@TerrDesc", SqlDbType.NChar)
            myCmd.Parameters("@TerrDesc").Value = txtTerrDesc.Text

            myCmd.Parameters.Add("@Region", SqlDbType.Int)
            myCmd.Parameters("@Region").Value = Integer.Parse(txtRegionID.Text)

            Dim intRows As Integer = myCmd.ExecuteNonQuery()
            If intRows = 1 Then
                lblStatus.Text = "Update succeeded"
            Else
                lblStatus.Text = "Update failed"
            End If

        Catch ex As Exception
            MessageBox.Show("Update into Territories failed due to exception: " + ex.Message, "COMP10039 CH10")
        Finally
            myConnection.Close()
        End Try

    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim myConnection As SqlConnection = New SqlConnection(strConnection)

        Try
            myConnection.Open()
            Dim myCmd As SqlCommand = New SqlCommand()
            myCmd.Connection = myConnection
            myCmd.CommandText = "DELETE FROM Territories WHERE TerritoryID = @TerrID"

            myCmd.Parameters.Add("@TerrID", SqlDbType.NVarChar)
            myCmd.Parameters("@TerrID").Value = txtTerrID.Text

            Dim intRows As Integer = myCmd.ExecuteNonQuery()
            If intRows = 1 Then
                lblStatus.Text = "Delete succeeded"
            Else
                lblStatus.Text = "Delete failed"
            End If

        Catch ex As Exception
            MessageBox.Show("Delete from Territories failed due to exception: " + ex.Message, "COMP10039 CH10")
        Finally
            myConnection.Close()
        End Try

    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtRegionID.ResetText()
        txtTerrDesc.ResetText()
        txtTerrID.ResetText()
    End Sub
End Class
