﻿Public Class Form1
    Const intLOTTERY_NUMBERS_MAX_SUBSCRIPT As Integer = 5
    Const intMAX_NUMBER As Integer = 49
    Const intMIN_NUMBER As Integer = 1

    Private Sub btnGenerate_Click(sender As Object, e As EventArgs) Handles btnGenerate.Click
        Dim intNumbers() As Integer
        intNumbers = intLotteryNumbers()

        'Find the max value in our array
        Dim intMin As Integer = intFindMin(intNumbers)
        Dim intMax As Integer = intFindMax(intNumbers)
        Dim dblAverage As Double = dblCalculateAverage(intNumbers)

    End Sub

    Function intFindMin(intValues() As Integer) As Integer
        'Find the min value in our array
        Dim intMin As Integer = intMAX_NUMBER
        For Each intValue As Integer In intValues
            If intValue < intMin Then
                intMin = intValue
            End If
        Next

        Array.Sort(intValues)

        'This will NOT change what the callers array points to since intValues is called by value
        Dim intTempArray() As Integer = {1, 2, 3}
        intValues = intTempArray

        Return intMin
    End Function

    Function intFindMax(ByRef intValues() As Integer) As Integer
        'Find the min value in our array
        Dim intMax As Integer = intMIN_NUMBER
        For Each intValue As Integer In intValues
            If intValue > intMax Then
                intMax = intValue
            End If
        Next

        Array.Sort(intValues)

        Dim intTempArray() As Integer = {1, 2, 3}
        'This will change what the callers array points to since intValues is called by reference
        intValues = intTempArray

        Return intMax
    End Function

    Function dblCalculateAverage(intValues() As Integer) As Double
        'Find the average
        Dim dblAverage As Double
        For Each intValue As Integer In intValues
            dblAverage += intValue
        Next
        dblAverage /= intValues.Length

        Return dblAverage
    End Function

    Function intLotteryNumbers() As Integer()
        Dim rndNumber As New Random
        Dim intCounter As Integer
        Dim intNumbers(intLOTTERY_NUMBERS_MAX_SUBSCRIPT) As Integer

        For intCounter = 0 To intLOTTERY_NUMBERS_MAX_SUBSCRIPT 'intNumbers.Length - 1
            intNumbers(intCounter) = rndNumber.Next(intMIN_NUMBER, intMAX_NUMBER)
            lstNumbers.Items.Add(intNumbers(intCounter).ToString())
        Next

        Return intNumbers
    End Function

    Private Sub btn2DArray_Click(sender As Object, e As EventArgs) Handles btn2DArray.Click
        Dim intVotes(,) As Integer = {{10, 20, 25, 45}, 'Trump
                                       {25, 20, 10, 145}, 'Clinton
                                       {100, 50, 40, 110} 'Johnson 
                                     }
        Dim intTotals() As Integer = {0, 0, 0}
        'Dim intTest(3) As Integer

        Dim intRow, intColumn As Integer

        For intRow = 0 To intVotes.GetLength(0) - 1
            Dim intTotal As Integer = 0

            For intColumn = 0 To intVotes.GetLength(1) - 1
                intTotal += intVotes(intRow, intColumn)
            Next
            intTotals(intRow) = intTotal
        Next

        Dim intTotalVoters As Integer
        For Each intCount As Integer In intVotes
            intTotalVoters += intCount
        Next
    End Sub

    Private Sub radStudent_CheckedChanged(sender As Object, e As EventArgs) Handles radStudent.CheckedChanged
        If radStudent.Checked Then
            lstStudentType.Enabled = True
        Else
            lstStudentType.Enabled = False
        End If
    End Sub

    Private Sub tmrTime_Tick(sender As Object, e As EventArgs) Handles tmrTime.Tick
        lblTime.Text = Now.ToLongTimeString()
    End Sub
End Class
