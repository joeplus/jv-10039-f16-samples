﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblSalesFL = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnSalesForLoop = New System.Windows.Forms.Button()
        Me.lblSalesDW = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnSalesDoWhile = New System.Windows.Forms.Button()
        Me.lblTotalSales = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnSales = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnLoadExit = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.nudRate = New System.Windows.Forms.NumericUpDown()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.nudDown = New System.Windows.Forms.NumericUpDown()
        Me.nudPrice = New System.Windows.Forms.NumericUpDown()
        Me.nudMonths = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lsbPayments = New System.Windows.Forms.ListBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.lsbStudents = New System.Windows.Forms.ListBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnAddStudent = New System.Windows.Forms.Button()
        Me.lsbMembers = New System.Windows.Forms.ListBox()
        Me.btnRemoveStudent = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudRate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDown, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudPrice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(13, 13)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1139, 717)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.lblSalesFL)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.btnSalesForLoop)
        Me.TabPage1.Controls.Add(Me.lblSalesDW)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.btnSalesDoWhile)
        Me.TabPage1.Controls.Add(Me.lblTotalSales)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.btnSales)
        Me.TabPage1.Location = New System.Drawing.Point(4, 33)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1131, 680)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Loops"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(28, 51)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(204, 24)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Do While Posttest Loop"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(30, 165)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(202, 24)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Do While Pretest Loop:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(96, 285)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(136, 24)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "For Next Loop:"
        '
        'lblSalesFL
        '
        Me.lblSalesFL.AutoSize = True
        Me.lblSalesFL.Location = New System.Drawing.Point(498, 285)
        Me.lblSalesFL.Name = "lblSalesFL"
        Me.lblSalesFL.Size = New System.Drawing.Size(0, 24)
        Me.lblSalesFL.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(385, 285)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 24)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Sales Total:"
        '
        'btnSalesForLoop
        '
        Me.btnSalesForLoop.Location = New System.Drawing.Point(238, 275)
        Me.btnSalesForLoop.Name = "btnSalesForLoop"
        Me.btnSalesForLoop.Size = New System.Drawing.Size(123, 44)
        Me.btnSalesForLoop.TabIndex = 6
        Me.btnSalesForLoop.Text = "Sales Entry"
        Me.btnSalesForLoop.UseVisualStyleBackColor = True
        '
        'lblSalesDW
        '
        Me.lblSalesDW.AutoSize = True
        Me.lblSalesDW.Location = New System.Drawing.Point(498, 165)
        Me.lblSalesDW.Name = "lblSalesDW"
        Me.lblSalesDW.Size = New System.Drawing.Size(0, 24)
        Me.lblSalesDW.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(385, 165)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(107, 24)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Sales Total:"
        '
        'btnSalesDoWhile
        '
        Me.btnSalesDoWhile.Location = New System.Drawing.Point(238, 155)
        Me.btnSalesDoWhile.Name = "btnSalesDoWhile"
        Me.btnSalesDoWhile.Size = New System.Drawing.Size(123, 44)
        Me.btnSalesDoWhile.TabIndex = 3
        Me.btnSalesDoWhile.Text = "Sales Entry"
        Me.btnSalesDoWhile.UseVisualStyleBackColor = True
        '
        'lblTotalSales
        '
        Me.lblTotalSales.AutoSize = True
        Me.lblTotalSales.Location = New System.Drawing.Point(498, 51)
        Me.lblTotalSales.Name = "lblTotalSales"
        Me.lblTotalSales.Size = New System.Drawing.Size(0, 24)
        Me.lblTotalSales.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(385, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(107, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Sales Total:"
        '
        'btnSales
        '
        Me.btnSales.Location = New System.Drawing.Point(238, 41)
        Me.btnSales.Name = "btnSales"
        Me.btnSales.Size = New System.Drawing.Size(123, 44)
        Me.btnSales.TabIndex = 0
        Me.btnSales.Text = "Sales Entry"
        Me.btnSales.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.ComboBox3)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.ComboBox2)
        Me.TabPage2.Controls.Add(Me.ComboBox1)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.ListBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 33)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1131, 680)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "ListBox and ComboBox"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(409, 291)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(232, 24)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "DropDownList ComboBox:"
        '
        'ComboBox3
        '
        Me.ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Items.AddRange(New Object() {"Hamilton", "Ancaster", "Stoney Creek", "Brantford", "Burlington"})
        Me.ComboBox3.Location = New System.Drawing.Point(647, 288)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(151, 32)
        Me.ComboBox3.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(362, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(279, 24)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "DropDown ComboBox (Default):"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"Hamilton", "Ancaster", "Stoney Creek", "Brantford", "Burlington"})
        Me.ComboBox2.Location = New System.Drawing.Point(647, 43)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(151, 32)
        Me.ComboBox2.TabIndex = 4
        '
        'ComboBox1
        '
        Me.ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"Hamilton", "Ancaster", "Stoney Creek", "Brantford", "Burlington"})
        Me.ComboBox1.Location = New System.Drawing.Point(647, 95)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(151, 174)
        Me.ComboBox1.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(468, 98)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(173, 24)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Simple ComboBox:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 24)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "ListBox:"
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 24
        Me.ListBox1.Items.AddRange(New Object() {"Hamilton", "Ancaster", "Stoney Creek", "Brantford", "Burlington"})
        Me.ListBox1.Location = New System.Drawing.Point(106, 46)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(151, 100)
        Me.ListBox1.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnLoadExit)
        Me.TabPage3.Controls.Add(Me.btnClear)
        Me.TabPage3.Controls.Add(Me.btnCalculate)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 33)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1131, 680)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Car Loan"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btnLoadExit
        '
        Me.btnLoadExit.Location = New System.Drawing.Point(548, 609)
        Me.btnLoadExit.Name = "btnLoadExit"
        Me.btnLoadExit.Size = New System.Drawing.Size(124, 49)
        Me.btnLoadExit.TabIndex = 5
        Me.btnLoadExit.Text = "Exit"
        Me.btnLoadExit.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(290, 609)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(124, 49)
        Me.btnClear.TabIndex = 4
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(32, 609)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(124, 49)
        Me.btnCalculate.TabIndex = 2
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.nudRate)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.nudDown)
        Me.GroupBox1.Controls.Add(Me.nudPrice)
        Me.GroupBox1.Controls.Add(Me.nudMonths)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(32, 24)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(321, 181)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Vehicle & Loan Information"
        '
        'nudRate
        '
        Me.nudRate.DecimalPlaces = 2
        Me.nudRate.Location = New System.Drawing.Point(192, 146)
        Me.nudRate.Name = "nudRate"
        Me.nudRate.Size = New System.Drawing.Size(120, 29)
        Me.nudRate.TabIndex = 7
        Me.nudRate.Value = New Decimal(New Integer() {500, 0, 0, 131072})
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(36, 148)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(150, 24)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "Interest Rate (%):"
        '
        'nudDown
        '
        Me.nudDown.DecimalPlaces = 2
        Me.nudDown.Increment = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudDown.Location = New System.Drawing.Point(192, 69)
        Me.nudDown.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudDown.Minimum = New Decimal(New Integer() {100, 0, 0, 0})
        Me.nudDown.Name = "nudDown"
        Me.nudDown.Size = New System.Drawing.Size(120, 29)
        Me.nudDown.TabIndex = 5
        Me.nudDown.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'nudPrice
        '
        Me.nudPrice.DecimalPlaces = 2
        Me.nudPrice.Increment = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudPrice.Location = New System.Drawing.Point(192, 33)
        Me.nudPrice.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.nudPrice.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudPrice.Name = "nudPrice"
        Me.nudPrice.Size = New System.Drawing.Size(120, 29)
        Me.nudPrice.TabIndex = 4
        Me.nudPrice.Value = New Decimal(New Integer() {10100, 0, 0, 0})
        '
        'nudMonths
        '
        Me.nudMonths.Location = New System.Drawing.Point(192, 108)
        Me.nudMonths.Maximum = New Decimal(New Integer() {72, 0, 0, 0})
        Me.nudMonths.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMonths.Name = "nudMonths"
        Me.nudMonths.Size = New System.Drawing.Size(120, 29)
        Me.nudMonths.TabIndex = 3
        Me.nudMonths.Value = New Decimal(New Integer() {48, 0, 0, 0})
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(15, 110)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(171, 24)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Number of Months:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(44, 71)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(142, 24)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Down Payment:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(128, 35)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 24)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Price:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lsbPayments)
        Me.GroupBox2.Location = New System.Drawing.Point(32, 211)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(820, 378)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Interest and Principle Payments"
        '
        'lsbPayments
        '
        Me.lsbPayments.FormattingEnabled = True
        Me.lsbPayments.ItemHeight = 24
        Me.lsbPayments.Location = New System.Drawing.Point(6, 28)
        Me.lsbPayments.Name = "lsbPayments"
        Me.lsbPayments.Size = New System.Drawing.Size(799, 340)
        Me.lsbPayments.TabIndex = 1
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnRemoveStudent)
        Me.TabPage4.Controls.Add(Me.lsbMembers)
        Me.TabPage4.Controls.Add(Me.btnAddStudent)
        Me.TabPage4.Controls.Add(Me.Label15)
        Me.TabPage4.Controls.Add(Me.lsbStudents)
        Me.TabPage4.Location = New System.Drawing.Point(4, 33)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1131, 680)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "PC5 - Club"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'lsbStudents
        '
        Me.lsbStudents.FormattingEnabled = True
        Me.lsbStudents.ItemHeight = 24
        Me.lsbStudents.Items.AddRange(New Object() {"Adams, Ben", "Baker, Sally", "Canseco, Juan", "Davis, Sharon", "Etienne, Jean", "Gonzalez, Jose", "Johnson, Eric", "Koenig, Johann"})
        Me.lsbStudents.Location = New System.Drawing.Point(35, 56)
        Me.lsbStudents.Name = "lsbStudents"
        Me.lsbStudents.Size = New System.Drawing.Size(227, 292)
        Me.lsbStudents.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(35, 25)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(178, 24)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "General Student List"
        '
        'btnAddStudent
        '
        Me.btnAddStudent.Location = New System.Drawing.Point(268, 133)
        Me.btnAddStudent.Name = "btnAddStudent"
        Me.btnAddStudent.Size = New System.Drawing.Size(118, 121)
        Me.btnAddStudent.TabIndex = 2
        Me.btnAddStudent.Text = "Add selected student"
        Me.btnAddStudent.UseVisualStyleBackColor = True
        '
        'lsbMembers
        '
        Me.lsbMembers.FormattingEnabled = True
        Me.lsbMembers.ItemHeight = 24
        Me.lsbMembers.Location = New System.Drawing.Point(491, 56)
        Me.lsbMembers.Name = "lsbMembers"
        Me.lsbMembers.Size = New System.Drawing.Size(205, 292)
        Me.lsbMembers.TabIndex = 3
        '
        'btnRemoveStudent
        '
        Me.btnRemoveStudent.Location = New System.Drawing.Point(702, 133)
        Me.btnRemoveStudent.Name = "btnRemoveStudent"
        Me.btnRemoveStudent.Size = New System.Drawing.Size(118, 121)
        Me.btnRemoveStudent.TabIndex = 4
        Me.btnRemoveStudent.Text = "Remove selected student"
        Me.btnRemoveStudent.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1164, 742)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "COMP10039 - JV F16 Chapter 5 Samples"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudRate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDown, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudPrice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMonths, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents btnSales As Button
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents lblTotalSales As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblSalesFL As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btnSalesForLoop As Button
    Friend WithEvents lblSalesDW As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnSalesDoWhile As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents ComboBox3 As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents btnCalculate As Button
    Friend WithEvents lsbPayments As ListBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents nudDown As NumericUpDown
    Friend WithEvents nudPrice As NumericUpDown
    Friend WithEvents nudMonths As NumericUpDown
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents nudRate As NumericUpDown
    Friend WithEvents Label14 As Label
    Friend WithEvents btnLoadExit As Button
    Friend WithEvents btnClear As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents btnRemoveStudent As Button
    Friend WithEvents lsbMembers As ListBox
    Friend WithEvents btnAddStudent As Button
    Friend WithEvents Label15 As Label
    Friend WithEvents lsbStudents As ListBox
End Class
