﻿Public Class Form1
    Private Sub btnSales_Click(sender As Object, e As EventArgs) Handles btnSales.Click
        Const intTOTAL_DAYS As Integer = 5
        Dim strDailySales As String
        Dim decDailySales As Decimal
        Dim decTotalSales As Decimal
        Dim intCounter As Integer = 1

        Do
            strDailySales = InputBox("Enter the sales for the day: ", "COMP10039 Week5")
            If (Decimal.TryParse(strDailySales, decDailySales)) Then
                decTotalSales += decDailySales
                intCounter += 1
            Else
                MessageBox.Show("Please enter a valid number", "COMP10039 Week 5")
            End If
        Loop While intCounter <= intTOTAL_DAYS

        lblTotalSales.Text = decTotalSales.ToString("C")
    End Sub

    Private Sub btnSalesDoWhile_Click(sender As Object, e As EventArgs) Handles btnSalesDoWhile.Click
        Const intTOTAL_DAYS As Integer = 5
        Dim strDailySales As String
        Dim decDailySales As Decimal
        Dim decTotalSales As Decimal
        Dim intCounter As Integer = 1

        Do While intCounter <= intTOTAL_DAYS
            strDailySales = InputBox("Enter the sales for the day: ", "COMP10039 Week5")
            If (Decimal.TryParse(strDailySales, decDailySales)) Then
                decTotalSales += decDailySales
                intCounter += 1
            Else
                MessageBox.Show("Please enter a valid number", "COMP10039 Week 5")
            End If
        Loop

        lblSalesDW.Text = decTotalSales.ToString("C")

    End Sub

    Private Sub btnSalesForLoop_Click(sender As Object, e As EventArgs) Handles btnSalesForLoop.Click
        Const intTOTAL_DAYS As Integer = 5
        Dim strDailySales As String
        Dim decDailySales As Decimal
        Dim decTotalSales As Decimal
        Dim intCounter As Integer = 1

        'Do While intCounter <= intTOTAL_DAYS
        For intCounter = 1 To intTOTAL_DAYS
            strDailySales = InputBox("Enter the sales for the day: ", "COMP10039 Week5")
            If (Decimal.TryParse(strDailySales, decDailySales)) Then
                decTotalSales += decDailySales
            Else
                MessageBox.Show("Please enter a valid number", "COMP10039 Week 5")
                intCounter -= 1
                'Exit For - to stop iterating, immediately exit the loop and continue with the first statement after the loop
                Continue For ' To skip the remaining instructions for this iteration of the loop
            End If
        Next
        'Loop
        lblSalesFL.Text = decTotalSales.ToString("C")
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Dim intCounter As Integer = 1
        Dim decLoanAmount As Decimal = nudPrice.Value - nudDown.Value
        Dim decMonthlyRate As Decimal = nudRate.Value / 1200
        Dim decMonthlyPayment As Decimal = decCalculateMonthlyPayment(decLoanAmount, nudMonths.Value, decMonthlyRate)
        decMonthlyPayment = Decimal.Round(decMonthlyPayment, 2)
        lsbPayments.Items.Clear()

        Dim decTotalPayments, decTotalInterest, decTotalPrinciple As Decimal
        For intCounter = 1 To nudMonths.Value

            Dim decInterest As Decimal = decBalance(decLoanAmount, nudMonths.Value, intCounter - 1, decMonthlyRate) * decMonthlyRate
            decInterest = Decimal.Round(decInterest, 2)
            Dim decPrincipal As Decimal = decMonthlyPayment - decInterest

            Dim strPaymentInfo As String
            strPaymentInfo = "Payment: " + decMonthlyPayment.ToString("C") _
                              + "; Principal: " + decPrincipal.ToString("C") _
                                + "; Interest: " + decInterest.ToString("C")
            lsbPayments.Items.Add(strPaymentInfo)

            decTotalPayments += decMonthlyPayment
            decTotalPrinciple += decPrincipal
            decTotalInterest += decInterest
        Next

        Dim strTotals As String
        strTotals = "***** Total Payments: " + decTotalPayments.ToString("C") _
                              + "; Total Principal: " + decTotalPrinciple.ToString("C") _
                                + "; Total Interest: " + decTotalInterest.ToString("C")
        lsbPayments.Items.Add(strTotals)
    End Sub

    Private Function decCalculateMonthlyPayment(decLoan As Decimal, intMonths As Integer, decMonthlyRate As Decimal) As Decimal
        'P = L[c(1 + c)^n]/[(1 + c)n - 1]
        Dim decPayment As Decimal = decLoan * (decMonthlyRate * (1 + decMonthlyRate) ^ intMonths) / ((1 + decMonthlyRate) ^ intMonths - 1)
        Return decPayment
    End Function

    Private Function decBalance(decLoan As Decimal, intTotalMonths As Integer, intMonthsIn As Integer, decMonthlyRate As Decimal) As Decimal
        'B = L[(1 + c)n - (1 + c)p]/[(1 + c)n - 1]
        Dim decRemainingBalance As Decimal = decLoan * (((1 + decMonthlyRate) ^ intTotalMonths) - ((1 + decMonthlyRate) ^ intMonthsIn)) / ((1 + decMonthlyRate) ^ intTotalMonths - 1)
        Return decRemainingBalance
    End Function

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        lsbPayments.Items.Clear()
    End Sub

    Private Sub btnLoadExit_Click(sender As Object, e As EventArgs) Handles btnLoadExit.Click
        Close()
    End Sub

    Private Sub btnAddStudent_Click(sender As Object, e As EventArgs) Handles btnAddStudent.Click
        If (lsbStudents.SelectedIndex = -1) Then
            MessageBox.Show("Please select a student to add to the club", "COMP10039 Ch5")
            Return
        Else
            If lsbMembers.Items.Contains(lsbStudents.SelectedItem) Then
                MessageBox.Show("That student is already a member", "COMP10039 Ch5")
                Return
            End If
            lsbMembers.Items.Add(lsbStudents.SelectedItem)
        End If
    End Sub

    Private Sub btnRemoveStudent_Click(sender As Object, e As EventArgs) Handles btnRemoveStudent.Click
        If (lsbMembers.SelectedIndex = -1) Then
            MessageBox.Show("Please select a student to remove from the club", "COMP10039 Ch5")
            Return
        Else
            lsbMembers.Items.Remove(lsbMembers.SelectedItem)
        End If
    End Sub
End Class
