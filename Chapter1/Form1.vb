﻿Public Class Form1
    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        lblGreeting.Text = "Hello World!"
    End Sub

    Private Sub btnGrossPay_Click(sender As Object, e As EventArgs) Handles btnGrossPay.Click
        Dim HoursWorked As Double = Convert.ToDouble(txtHours.Text)
        Dim HourlyRate As Double = Convert.ToDouble(txtHourlyRate.Text)
        Dim GrossPay As Double = HoursWorked * HourlyRate
        lblGrossPay.Text = GrossPay.ToString("c")
    End Sub

    Private Sub btnPayExit_Click(sender As Object, e As EventArgs) Handles btnPayExit.Click
        Me.Close()
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub mtbPhone_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles mtbPhone.Validating
        If (mtbPhone.MaskCompleted) Then
            ErrorProvider1.SetError(mtbPhone, String.Empty)
        Else
            ErrorProvider1.SetError(mtbPhone, mtbPhone.Tag.ToString())
        End If
    End Sub
End Class
