﻿Public Class Form1
    Const WINTER_JACKET_TEMP As Double = 0.0
    Const LIGHT_JACKET_TEMP As Double = 10.0
    Const SWEATER_TEMP As Double = 20.0

    Const YEARLY_LEASE_COST As Decimal = 5000
    Const ONE_TIME_PURCHASE_COST As Decimal = 20000
    Const LEVEL3_SUPPORT_COST As Decimal = 3500
    Const ONSITE_TRAINING_COST As Decimal = 2000
    Const CLOUD_BACKUP_COST As Decimal = 300

    Private Sub btnRecommendation_Click(sender As Object, e As EventArgs) Handles btnRecommendation.Click
        Dim dblCurrentTemp As Double = nudTemp.Value

        If dblCurrentTemp < WINTER_JACKET_TEMP Then
            lblClothing.Text = "Wear a winter jacket"
        ElseIf dblCurrentTemp < LIGHT_JACKET_TEMP Then
            lblClothing.Text = "Wear a light jacket"
        ElseIf dblCurrentTemp < SWEATER_TEMP Then
            lblClothing.Text = "Wear a sweater"
        Else
            lblClothing.Text = "It's warm outside.  Put on your shorts!"
        End If
    End Sub

    Private Sub btnWeatherExit_Click(sender As Object, e As EventArgs) Handles btnWeatherExit.Click
        Close()
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        nudTemp.Value = WINTER_JACKET_TEMP
        lblClothing.Text = String.Empty
    End Sub

    Private Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        'We probably should have used the MOD operator to determine whether the number was 
        'even or odd but this was the example that the Textbook used so we went with it
        'We could have done the following:
        'If nudNumber.Value Mod 2 = 0 Then
        '  lblResult.Text = "That number is EVEN"
        'Else
        '  lblResult.Text = "That number is ODD"
        'End If

        Select Case nudNumber.Value
            Case 2, 4, 6, 8, 10
                lblResult.Text = "That number is EVEN"
            Case 1, 3, 5, 7, 9
                lblResult.Text = "That number is ODD"
                'Case Else is redundant as we have set the range of the NumericUpDown control
            Case Else
                lblResult.Text = "That number is INVALID"
        End Select
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSWCalculate_Click(sender As Object, e As EventArgs) Handles btnSWCalculate.Click
        Dim decSWCost, decOptionsCost As Decimal

        If rbYearly.Checked Then
            decSWCost = YEARLY_LEASE_COST
        Else
            decSWCost = ONE_TIME_PURCHASE_COST
        End If

        If chkL3Support.Checked Then
            decOptionsCost = LEVEL3_SUPPORT_COST
        End If
        If chkTraining.Checked Then
            decOptionsCost += ONSITE_TRAINING_COST
        End If
        If chkBackup.Checked Then
            decOptionsCost += CLOUD_BACKUP_COST
        End If

        lblSWCost.Text = decSWCost.ToString("C")
        lblOptionsCost.Text = decOptionsCost.ToString("C")
    End Sub

    Private Sub btnSWClear_Click(sender As Object, e As EventArgs) Handles btnSWClear.Click
        rbYearly.Checked = True
        chkBackup.Checked = False
        chkL3Support.Checked = False
        chkTraining.Checked = False
        btnSWCalculate_Click(sender, e)
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub btnTempCalculate_Click(sender As Object, e As EventArgs) Handles btnTempCalculate.Click
        Dim dblTempAverage As Double

        If IsTemperatureValid(txtWeek1, "Week 1") AndAlso IsTemperatureValid(txtWeek2, "Week 2") AndAlso IsTemperatureValid(txtWeek3, "Week 3") _
           AndAlso IsTemperatureValid(txtWeek4, "Week 4") AndAlso IsTemperatureValid(txtWeek5, "Week 5") Then
            dblTempAverage = (CDbl(txtWeek1.Text) + CDbl(txtWeek2.Text) + CDbl(txtWeek3.Text) + CDbl(txtWeek4.Text) + CDbl(txtWeek5.Text)) / 5
            lblAverage.Text = dblTempAverage.ToString("N")
        Else
            lblAverage.Text = "Invalid"
        End If
    End Sub

    Private Function IsTemperatureValid(txtTemperature As TextBox, strTxtName As String) As Boolean
        If IsNumeric(txtTemperature.Text) Then
            If CDbl(txtTemperature.Text) < -50 Then
                ToolStripStatusLabel1.Text = "Temperature field " + strTxtName + " is below the minimum of -50.  Please try again."
            ElseIf CDbl(txtTemperature.Text) > 130 Then
                ToolStripStatusLabel1.Text = "Temperature field " + strTxtName + " is above the maximum of 130.  Please try again."
            Else
                ToolStripStatusLabel1.Text = String.Empty
                Return True
            End If
        Else
            ToolStripStatusLabel1.Text = "Temperature field " + strTxtName + " is not a valid number.  Please try again."
        End If

        txtTemperature.Focus()
        Return False
    End Function

    Private Sub btnTempClear_Click(sender As Object, e As EventArgs) Handles btnTempClear.Click
        txtWeek1.ResetText()
        txtWeek2.ResetText()
        txtWeek3.ResetText()
        txtWeek4.ResetText()
        txtWeek5.ResetText()
        lblAverage.ResetText()
        ToolStripStatusLabel1.Text = String.Empty
    End Sub

    Private Sub btnTempExit_Click(sender As Object, e As EventArgs) Handles btnTempExit.Click
        Close()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnCalculateGrade_Click(sender As Object, e As EventArgs) Handles btnCalculateGrade.Click
        Select Case nudGrade.Value
            Case Is >= 90
                lblLetterGrade.Text = "A+"
            Case 80 To 89
                lblLetterGrade.Text = "A"
            Case 70 To 79
                lblLetterGrade.Text = "B"
            Case 60 To 69
                lblLetterGrade.Text = "C"
            Case 50 To 59
                lblLetterGrade.Text = "D"
            Case Else
                lblLetterGrade.Text = "F"
        End Select
    End Sub

    Private Sub bntCalculate_Click(sender As Object, e As EventArgs) Handles bntCalculate.Click
        Dim decMonthly As Decimal

        If radChild.Checked Then
            decMonthly = 10
        ElseIf radSenior.Checked Then
            decMonthly = 15
        ElseIf radStudent.Checked Then
            decMonthly = 12
        Else
            decMonthly = 30
        End If

        Dim decOptions As Decimal
        If chkYoga.Checked Then
            decOptions += 10
        End If
        If chkKarate.Checked Then
            decOptions += 30
        End If
        If chkTrainer.Checked Then
            decOptions += 50
        End If

        lblMonthlyFee.Text = (decMonthly + decOptions).ToString("C")
        Dim decTotal As Decimal = (decMonthly + decOptions) * nudMonths.Value
        lblTotalFee.Text = decTotal.ToString("C")
    End Sub
End Class
