﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnWeatherExit = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnRecommendation = New System.Windows.Forms.Button()
        Me.lblClothing = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nudTemp = New System.Windows.Forms.NumericUpDown()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnSubmit = New System.Windows.Forms.Button()
        Me.lblResult = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.nudNumber = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnTempExit = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.btnTempClear = New System.Windows.Forms.Button()
        Me.btnTempCalculate = New System.Windows.Forms.Button()
        Me.lblAverage = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtWeek5 = New System.Windows.Forms.TextBox()
        Me.txtWeek4 = New System.Windows.Forms.TextBox()
        Me.txtWeek3 = New System.Windows.Forms.TextBox()
        Me.txtWeek2 = New System.Windows.Forms.TextBox()
        Me.txtWeek1 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSWClear = New System.Windows.Forms.Button()
        Me.btnSWCalculate = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblOptionsCost = New System.Windows.Forms.Label()
        Me.lblSWCost = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkBackup = New System.Windows.Forms.CheckBox()
        Me.chkTraining = New System.Windows.Forms.CheckBox()
        Me.chkL3Support = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rbOneTime = New System.Windows.Forms.RadioButton()
        Me.rbYearly = New System.Windows.Forms.RadioButton()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.nudGrade = New System.Windows.Forms.NumericUpDown()
        Me.lblLetterGrade = New System.Windows.Forms.Label()
        Me.btnCalculateGrade = New System.Windows.Forms.Button()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.radAdult = New System.Windows.Forms.RadioButton()
        Me.radChild = New System.Windows.Forms.RadioButton()
        Me.radSenior = New System.Windows.Forms.RadioButton()
        Me.radStudent = New System.Windows.Forms.RadioButton()
        Me.chkYoga = New System.Windows.Forms.CheckBox()
        Me.chkKarate = New System.Windows.Forms.CheckBox()
        Me.chkTrainer = New System.Windows.Forms.CheckBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.nudMonths = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblMonthlyFee = New System.Windows.Forms.Label()
        Me.lblTotalFee = New System.Windows.Forms.Label()
        Me.bntCalculate = New System.Windows.Forms.Button()
        Me.btnFitnessClear = New System.Windows.Forms.Button()
        Me.btnFitnessExit = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.nudTemp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.nudNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.nudGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.nudMonths, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1140, 718)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnWeatherExit)
        Me.TabPage1.Controls.Add(Me.btnClear)
        Me.TabPage1.Controls.Add(Me.btnRecommendation)
        Me.TabPage1.Controls.Add(Me.lblClothing)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.nudTemp)
        Me.TabPage1.Location = New System.Drawing.Point(4, 33)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Weather"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnWeatherExit
        '
        Me.btnWeatherExit.Location = New System.Drawing.Point(458, 146)
        Me.btnWeatherExit.Name = "btnWeatherExit"
        Me.btnWeatherExit.Size = New System.Drawing.Size(181, 52)
        Me.btnWeatherExit.TabIndex = 6
        Me.btnWeatherExit.Text = "Exit"
        Me.btnWeatherExit.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(236, 146)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(181, 52)
        Me.btnClear.TabIndex = 5
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnRecommendation
        '
        Me.btnRecommendation.Location = New System.Drawing.Point(23, 146)
        Me.btnRecommendation.Name = "btnRecommendation"
        Me.btnRecommendation.Size = New System.Drawing.Size(181, 52)
        Me.btnRecommendation.TabIndex = 4
        Me.btnRecommendation.Text = "Recommendation"
        Me.btnRecommendation.UseVisualStyleBackColor = True
        '
        'lblClothing
        '
        Me.lblClothing.AutoSize = True
        Me.lblClothing.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblClothing.Location = New System.Drawing.Point(236, 84)
        Me.lblClothing.Name = "lblClothing"
        Me.lblClothing.Size = New System.Drawing.Size(2, 26)
        Me.lblClothing.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(221, 24)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Recommended Clothing:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(224, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Enter the Temp (Celsius):"
        '
        'nudTemp
        '
        Me.nudTemp.DecimalPlaces = 1
        Me.nudTemp.Location = New System.Drawing.Point(236, 39)
        Me.nudTemp.Maximum = New Decimal(New Integer() {70, 0, 0, 0})
        Me.nudTemp.Minimum = New Decimal(New Integer() {70, 0, 0, -2147483648})
        Me.nudTemp.Name = "nudTemp"
        Me.nudTemp.Size = New System.Drawing.Size(120, 29)
        Me.nudTemp.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnExit)
        Me.TabPage2.Controls.Add(Me.btnSubmit)
        Me.TabPage2.Controls.Add(Me.lblResult)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.nudNumber)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 33)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Select Case"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(206, 153)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(105, 41)
        Me.btnExit.TabIndex = 5
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSubmit
        '
        Me.btnSubmit.Location = New System.Drawing.Point(36, 153)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(105, 41)
        Me.btnSubmit.TabIndex = 4
        Me.btnSubmit.Text = "Submit"
        Me.btnSubmit.UseVisualStyleBackColor = True
        '
        'lblResult
        '
        Me.lblResult.AutoSize = True
        Me.lblResult.Location = New System.Drawing.Point(191, 79)
        Me.lblResult.Name = "lblResult"
        Me.lblResult.Size = New System.Drawing.Size(0, 24)
        Me.lblResult.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(118, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 24)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Result:"
        '
        'nudNumber
        '
        Me.nudNumber.Location = New System.Drawing.Point(191, 32)
        Me.nudNumber.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudNumber.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudNumber.Name = "nudNumber"
        Me.nudNumber.Size = New System.Drawing.Size(120, 29)
        Me.nudNumber.TabIndex = 1
        Me.nudNumber.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(153, 24)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Select a number:"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnTempExit)
        Me.TabPage4.Controls.Add(Me.StatusStrip1)
        Me.TabPage4.Controls.Add(Me.btnTempClear)
        Me.TabPage4.Controls.Add(Me.btnTempCalculate)
        Me.TabPage4.Controls.Add(Me.lblAverage)
        Me.TabPage4.Controls.Add(Me.Label12)
        Me.TabPage4.Controls.Add(Me.txtWeek5)
        Me.TabPage4.Controls.Add(Me.txtWeek4)
        Me.TabPage4.Controls.Add(Me.txtWeek3)
        Me.TabPage4.Controls.Add(Me.txtWeek2)
        Me.TabPage4.Controls.Add(Me.txtWeek1)
        Me.TabPage4.Controls.Add(Me.Label11)
        Me.TabPage4.Controls.Add(Me.Label10)
        Me.TabPage4.Controls.Add(Me.Label9)
        Me.TabPage4.Controls.Add(Me.Label8)
        Me.TabPage4.Controls.Add(Me.Label7)
        Me.TabPage4.Location = New System.Drawing.Point(4, 33)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "PC4 - Temperature"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnTempExit
        '
        Me.btnTempExit.Location = New System.Drawing.Point(323, 300)
        Me.btnTempExit.Name = "btnTempExit"
        Me.btnTempExit.Size = New System.Drawing.Size(102, 45)
        Me.btnTempExit.TabIndex = 15
        Me.btnTempExit.Text = "Exit"
        Me.btnTempExit.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 659)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(1132, 22)
        Me.StatusStrip1.TabIndex = 14
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 17)
        '
        'btnTempClear
        '
        Me.btnTempClear.Location = New System.Drawing.Point(179, 300)
        Me.btnTempClear.Name = "btnTempClear"
        Me.btnTempClear.Size = New System.Drawing.Size(102, 45)
        Me.btnTempClear.TabIndex = 13
        Me.btnTempClear.Text = "Clear"
        Me.btnTempClear.UseVisualStyleBackColor = True
        '
        'btnTempCalculate
        '
        Me.btnTempCalculate.Location = New System.Drawing.Point(33, 300)
        Me.btnTempCalculate.Name = "btnTempCalculate"
        Me.btnTempCalculate.Size = New System.Drawing.Size(102, 45)
        Me.btnTempCalculate.TabIndex = 12
        Me.btnTempCalculate.Text = "Calculate"
        Me.btnTempCalculate.UseVisualStyleBackColor = True
        '
        'lblAverage
        '
        Me.lblAverage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAverage.Location = New System.Drawing.Point(287, 230)
        Me.lblAverage.Name = "lblAverage"
        Me.lblAverage.Size = New System.Drawing.Size(100, 29)
        Me.lblAverage.TabIndex = 11
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(195, 231)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 24)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Average:"
        '
        'txtWeek5
        '
        Me.txtWeek5.Location = New System.Drawing.Point(288, 188)
        Me.txtWeek5.Name = "txtWeek5"
        Me.txtWeek5.Size = New System.Drawing.Size(100, 29)
        Me.txtWeek5.TabIndex = 9
        '
        'txtWeek4
        '
        Me.txtWeek4.Location = New System.Drawing.Point(288, 151)
        Me.txtWeek4.Name = "txtWeek4"
        Me.txtWeek4.Size = New System.Drawing.Size(100, 29)
        Me.txtWeek4.TabIndex = 8
        '
        'txtWeek3
        '
        Me.txtWeek3.Location = New System.Drawing.Point(288, 114)
        Me.txtWeek3.Name = "txtWeek3"
        Me.txtWeek3.Size = New System.Drawing.Size(100, 29)
        Me.txtWeek3.TabIndex = 7
        '
        'txtWeek2
        '
        Me.txtWeek2.Location = New System.Drawing.Point(288, 77)
        Me.txtWeek2.Name = "txtWeek2"
        Me.txtWeek2.Size = New System.Drawing.Size(100, 29)
        Me.txtWeek2.TabIndex = 6
        '
        'txtWeek1
        '
        Me.txtWeek1.Location = New System.Drawing.Point(288, 40)
        Me.txtWeek1.Name = "txtWeek1"
        Me.txtWeek1.Size = New System.Drawing.Size(100, 29)
        Me.txtWeek1.TabIndex = 5
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(29, 191)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(252, 24)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Week #5 Temp (Fahrenheit):"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(29, 154)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(252, 24)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Week #4 Temp (Fahrenheit):"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(29, 80)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(252, 24)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Week #2 Temp (Fahrenheit):"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(29, 117)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(252, 24)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Week #3 Temp (Fahrenheit):"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(29, 43)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(252, 24)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Week #1 Temp (Fahrenheit):"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnClose)
        Me.TabPage3.Controls.Add(Me.btnSWClear)
        Me.TabPage3.Controls.Add(Me.btnSWCalculate)
        Me.TabPage3.Controls.Add(Me.Panel1)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 33)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "PC5 - Software Sales"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(402, 399)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(105, 38)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSWClear
        '
        Me.btnSWClear.Location = New System.Drawing.Point(250, 399)
        Me.btnSWClear.Name = "btnSWClear"
        Me.btnSWClear.Size = New System.Drawing.Size(105, 38)
        Me.btnSWClear.TabIndex = 4
        Me.btnSWClear.Text = "Clear"
        Me.btnSWClear.UseVisualStyleBackColor = True
        '
        'btnSWCalculate
        '
        Me.btnSWCalculate.Location = New System.Drawing.Point(98, 399)
        Me.btnSWCalculate.Name = "btnSWCalculate"
        Me.btnSWCalculate.Size = New System.Drawing.Size(105, 38)
        Me.btnSWCalculate.TabIndex = 3
        Me.btnSWCalculate.Text = "Calculate"
        Me.btnSWCalculate.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblOptionsCost)
        Me.Panel1.Controls.Add(Me.lblSWCost)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Location = New System.Drawing.Point(98, 218)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(409, 140)
        Me.Panel1.TabIndex = 2
        '
        'lblOptionsCost
        '
        Me.lblOptionsCost.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOptionsCost.Location = New System.Drawing.Point(250, 70)
        Me.lblOptionsCost.Name = "lblOptionsCost"
        Me.lblOptionsCost.Size = New System.Drawing.Size(120, 30)
        Me.lblOptionsCost.TabIndex = 3
        '
        'lblSWCost
        '
        Me.lblSWCost.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblSWCost.Location = New System.Drawing.Point(250, 22)
        Me.lblSWCost.Name = "lblSWCost"
        Me.lblSWCost.Size = New System.Drawing.Size(120, 30)
        Me.lblSWCost.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(18, 71)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(214, 24)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Cost of optional features:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(226, 24)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Cost of software licensing:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkBackup)
        Me.GroupBox2.Controls.Add(Me.chkTraining)
        Me.GroupBox2.Controls.Add(Me.chkL3Support)
        Me.GroupBox2.Location = New System.Drawing.Point(315, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(303, 157)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Optional Features (yearly)"
        '
        'chkBackup
        '
        Me.chkBackup.AutoSize = True
        Me.chkBackup.Location = New System.Drawing.Point(19, 111)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.Size = New System.Drawing.Size(147, 28)
        Me.chkBackup.TabIndex = 2
        Me.chkBackup.Text = "Cloud Backup"
        Me.chkBackup.UseVisualStyleBackColor = True
        '
        'chkTraining
        '
        Me.chkTraining.AutoSize = True
        Me.chkTraining.Location = New System.Drawing.Point(19, 77)
        Me.chkTraining.Name = "chkTraining"
        Me.chkTraining.Size = New System.Drawing.Size(163, 28)
        Me.chkTraining.TabIndex = 1
        Me.chkTraining.Text = "On-site Training"
        Me.chkTraining.UseVisualStyleBackColor = True
        '
        'chkL3Support
        '
        Me.chkL3Support.AutoSize = True
        Me.chkL3Support.Location = New System.Drawing.Point(19, 43)
        Me.chkL3Support.Name = "chkL3Support"
        Me.chkL3Support.Size = New System.Drawing.Size(249, 28)
        Me.chkL3Support.TabIndex = 0
        Me.chkL3Support.Text = "Level-3 Technical Support"
        Me.chkL3Support.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbOneTime)
        Me.GroupBox1.Controls.Add(Me.rbYearly)
        Me.GroupBox1.Location = New System.Drawing.Point(19, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(250, 157)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Licensing Options"
        '
        'rbOneTime
        '
        Me.rbOneTime.AutoSize = True
        Me.rbOneTime.Location = New System.Drawing.Point(18, 76)
        Me.rbOneTime.Name = "rbOneTime"
        Me.rbOneTime.Size = New System.Drawing.Size(199, 28)
        Me.rbOneTime.TabIndex = 1
        Me.rbOneTime.TabStop = True
        Me.rbOneTime.Text = "One-Time Purchase"
        Me.rbOneTime.UseVisualStyleBackColor = True
        '
        'rbYearly
        '
        Me.rbYearly.AutoSize = True
        Me.rbYearly.Location = New System.Drawing.Point(18, 42)
        Me.rbYearly.Name = "rbYearly"
        Me.rbYearly.Size = New System.Drawing.Size(145, 28)
        Me.rbYearly.TabIndex = 0
        Me.rbYearly.TabStop = True
        Me.rbYearly.Text = "Yearly license"
        Me.rbYearly.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.btnCalculateGrade)
        Me.TabPage5.Controls.Add(Me.lblLetterGrade)
        Me.TabPage5.Controls.Add(Me.nudGrade)
        Me.TabPage5.Controls.Add(Me.Label13)
        Me.TabPage5.Location = New System.Drawing.Point(4, 33)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Grades"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(29, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(188, 24)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Enter your grade (%):"
        '
        'nudGrade
        '
        Me.nudGrade.Location = New System.Drawing.Point(223, 27)
        Me.nudGrade.Name = "nudGrade"
        Me.nudGrade.Size = New System.Drawing.Size(120, 29)
        Me.nudGrade.TabIndex = 1
        '
        'lblLetterGrade
        '
        Me.lblLetterGrade.AutoSize = True
        Me.lblLetterGrade.Location = New System.Drawing.Point(33, 84)
        Me.lblLetterGrade.Name = "lblLetterGrade"
        Me.lblLetterGrade.Size = New System.Drawing.Size(0, 24)
        Me.lblLetterGrade.TabIndex = 2
        '
        'btnCalculateGrade
        '
        Me.btnCalculateGrade.Location = New System.Drawing.Point(69, 168)
        Me.btnCalculateGrade.Name = "btnCalculateGrade"
        Me.btnCalculateGrade.Size = New System.Drawing.Size(148, 56)
        Me.btnCalculateGrade.TabIndex = 3
        Me.btnCalculateGrade.Text = "Calculate Letter Grade"
        Me.btnCalculateGrade.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.btnFitnessExit)
        Me.TabPage6.Controls.Add(Me.btnFitnessClear)
        Me.TabPage6.Controls.Add(Me.bntCalculate)
        Me.TabPage6.Controls.Add(Me.GroupBox6)
        Me.TabPage6.Controls.Add(Me.GroupBox5)
        Me.TabPage6.Controls.Add(Me.GroupBox4)
        Me.TabPage6.Controls.Add(Me.GroupBox3)
        Me.TabPage6.Location = New System.Drawing.Point(4, 33)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Fitness"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radStudent)
        Me.GroupBox3.Controls.Add(Me.radSenior)
        Me.GroupBox3.Controls.Add(Me.radChild)
        Me.GroupBox3.Controls.Add(Me.radAdult)
        Me.GroupBox3.Location = New System.Drawing.Point(30, 24)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(250, 184)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Type of Membership"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkTrainer)
        Me.GroupBox4.Controls.Add(Me.chkKarate)
        Me.GroupBox4.Controls.Add(Me.chkYoga)
        Me.GroupBox4.Location = New System.Drawing.Point(311, 24)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(302, 184)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Options"
        '
        'radAdult
        '
        Me.radAdult.AutoSize = True
        Me.radAdult.Checked = True
        Me.radAdult.Location = New System.Drawing.Point(16, 42)
        Me.radAdult.Name = "radAdult"
        Me.radAdult.Size = New System.Drawing.Size(71, 28)
        Me.radAdult.TabIndex = 0
        Me.radAdult.TabStop = True
        Me.radAdult.Text = "Adult"
        Me.radAdult.UseVisualStyleBackColor = True
        '
        'radChild
        '
        Me.radChild.AutoSize = True
        Me.radChild.Location = New System.Drawing.Point(16, 78)
        Me.radChild.Name = "radChild"
        Me.radChild.Size = New System.Drawing.Size(200, 28)
        Me.radChild.TabIndex = 1
        Me.radChild.Text = "Child (12 and under)"
        Me.radChild.UseVisualStyleBackColor = True
        '
        'radSenior
        '
        Me.radSenior.AutoSize = True
        Me.radSenior.Location = New System.Drawing.Point(17, 150)
        Me.radSenior.Name = "radSenior"
        Me.radSenior.Size = New System.Drawing.Size(199, 28)
        Me.radSenior.TabIndex = 2
        Me.radSenior.Text = "Senior (65 and over)"
        Me.radSenior.UseVisualStyleBackColor = True
        '
        'radStudent
        '
        Me.radStudent.AutoSize = True
        Me.radStudent.Location = New System.Drawing.Point(17, 114)
        Me.radStudent.Name = "radStudent"
        Me.radStudent.Size = New System.Drawing.Size(92, 28)
        Me.radStudent.TabIndex = 3
        Me.radStudent.Text = "Student"
        Me.radStudent.UseVisualStyleBackColor = True
        '
        'chkYoga
        '
        Me.chkYoga.AutoSize = True
        Me.chkYoga.Location = New System.Drawing.Point(24, 42)
        Me.chkYoga.Name = "chkYoga"
        Me.chkYoga.Size = New System.Drawing.Size(73, 28)
        Me.chkYoga.TabIndex = 0
        Me.chkYoga.Text = "Yoga"
        Me.chkYoga.UseVisualStyleBackColor = True
        '
        'chkKarate
        '
        Me.chkKarate.AutoSize = True
        Me.chkKarate.Location = New System.Drawing.Point(24, 79)
        Me.chkKarate.Name = "chkKarate"
        Me.chkKarate.Size = New System.Drawing.Size(82, 28)
        Me.chkKarate.TabIndex = 1
        Me.chkKarate.Text = "Karate"
        Me.chkKarate.UseVisualStyleBackColor = True
        '
        'chkTrainer
        '
        Me.chkTrainer.AutoSize = True
        Me.chkTrainer.Location = New System.Drawing.Point(24, 115)
        Me.chkTrainer.Name = "chkTrainer"
        Me.chkTrainer.Size = New System.Drawing.Size(168, 28)
        Me.chkTrainer.TabIndex = 2
        Me.chkTrainer.Text = "Personal Trainer"
        Me.chkTrainer.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.nudMonths)
        Me.GroupBox5.Controls.Add(Me.Label14)
        Me.GroupBox5.Location = New System.Drawing.Point(30, 240)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(250, 113)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Membership Length"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(16, 45)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(171, 24)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Number of Months:"
        '
        'nudMonths
        '
        Me.nudMonths.Location = New System.Drawing.Point(193, 43)
        Me.nudMonths.Maximum = New Decimal(New Integer() {36, 0, 0, 0})
        Me.nudMonths.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudMonths.Name = "nudMonths"
        Me.nudMonths.Size = New System.Drawing.Size(51, 29)
        Me.nudMonths.TabIndex = 1
        Me.nudMonths.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.lblTotalFee)
        Me.GroupBox6.Controls.Add(Me.lblMonthlyFee)
        Me.GroupBox6.Controls.Add(Me.Label16)
        Me.GroupBox6.Controls.Add(Me.Label15)
        Me.GroupBox6.Location = New System.Drawing.Point(311, 240)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(302, 113)
        Me.GroupBox6.TabIndex = 3
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Fees"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(20, 45)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 24)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Monthly Fee:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(84, 86)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(56, 24)
        Me.Label16.TabIndex = 1
        Me.Label16.Text = "Total:"
        '
        'lblMonthlyFee
        '
        Me.lblMonthlyFee.AutoSize = True
        Me.lblMonthlyFee.Location = New System.Drawing.Point(146, 45)
        Me.lblMonthlyFee.Name = "lblMonthlyFee"
        Me.lblMonthlyFee.Size = New System.Drawing.Size(0, 24)
        Me.lblMonthlyFee.TabIndex = 2
        '
        'lblTotalFee
        '
        Me.lblTotalFee.AutoSize = True
        Me.lblTotalFee.Location = New System.Drawing.Point(146, 86)
        Me.lblTotalFee.Name = "lblTotalFee"
        Me.lblTotalFee.Size = New System.Drawing.Size(0, 24)
        Me.lblTotalFee.TabIndex = 3
        '
        'bntCalculate
        '
        Me.bntCalculate.Location = New System.Drawing.Point(30, 397)
        Me.bntCalculate.Name = "bntCalculate"
        Me.bntCalculate.Size = New System.Drawing.Size(122, 53)
        Me.bntCalculate.TabIndex = 4
        Me.bntCalculate.Text = "Calculate"
        Me.bntCalculate.UseVisualStyleBackColor = True
        '
        'btnFitnessClear
        '
        Me.btnFitnessClear.Location = New System.Drawing.Point(257, 397)
        Me.btnFitnessClear.Name = "btnFitnessClear"
        Me.btnFitnessClear.Size = New System.Drawing.Size(122, 53)
        Me.btnFitnessClear.TabIndex = 5
        Me.btnFitnessClear.Text = "Clear"
        Me.btnFitnessClear.UseVisualStyleBackColor = True
        '
        'btnFitnessExit
        '
        Me.btnFitnessExit.Location = New System.Drawing.Point(491, 397)
        Me.btnFitnessExit.Name = "btnFitnessExit"
        Me.btnFitnessExit.Size = New System.Drawing.Size(122, 53)
        Me.btnFitnessExit.TabIndex = 6
        Me.btnFitnessExit.Text = "Exit"
        Me.btnFitnessExit.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1164, 742)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "COMP10039 - JV F16 Chapter 4 Samples"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.nudTemp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.nudNumber, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.nudGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.nudMonths, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnWeatherExit As Button
    Friend WithEvents btnClear As Button
    Friend WithEvents btnRecommendation As Button
    Friend WithEvents lblClothing As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents nudTemp As NumericUpDown
    Friend WithEvents btnExit As Button
    Friend WithEvents btnSubmit As Button
    Friend WithEvents lblResult As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents nudNumber As NumericUpDown
    Friend WithEvents Label3 As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents btnClose As Button
    Friend WithEvents btnSWClear As Button
    Friend WithEvents btnSWCalculate As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblOptionsCost As Label
    Friend WithEvents lblSWCost As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkBackup As CheckBox
    Friend WithEvents chkTraining As CheckBox
    Friend WithEvents chkL3Support As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents rbOneTime As RadioButton
    Friend WithEvents rbYearly As RadioButton
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents btnTempClear As Button
    Friend WithEvents btnTempCalculate As Button
    Friend WithEvents lblAverage As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtWeek5 As TextBox
    Friend WithEvents txtWeek4 As TextBox
    Friend WithEvents txtWeek3 As TextBox
    Friend WithEvents txtWeek2 As TextBox
    Friend WithEvents txtWeek1 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents btnTempExit As Button
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents btnCalculateGrade As Button
    Friend WithEvents lblLetterGrade As Label
    Friend WithEvents nudGrade As NumericUpDown
    Friend WithEvents Label13 As Label
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents btnFitnessExit As Button
    Friend WithEvents btnFitnessClear As Button
    Friend WithEvents bntCalculate As Button
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents lblTotalFee As Label
    Friend WithEvents lblMonthlyFee As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents nudMonths As NumericUpDown
    Friend WithEvents Label14 As Label
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents chkTrainer As CheckBox
    Friend WithEvents chkKarate As CheckBox
    Friend WithEvents chkYoga As CheckBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents radStudent As RadioButton
    Friend WithEvents radSenior As RadioButton
    Friend WithEvents radChild As RadioButton
    Friend WithEvents radAdult As RadioButton
End Class
