﻿Public Class MainForm
    Private Sub btnModal_Click(sender As Object, e As EventArgs) Handles btnModal.Click
        Dim frmSimple As New SimpleForm
        frmSimple.ShowDialog()

        Dim intCounter As Integer
        For intCounter = 1 To 10
            lstNumbers.Items.Add(intCounter.ToString())
        Next
    End Sub

    Private Sub btnModeless_Click(sender As Object, e As EventArgs) Handles btnModeless.Click
        Dim frmSimple As New SimpleForm
        frmSimple.Show()

        Dim intCounter As Integer
        For intCounter = 1 To 10
            lstNumbers.Items.Add(intCounter.ToString())
        Next
    End Sub

    Private Sub btnClearList_Click(sender As Object, e As EventArgs) Handles btnClearList.Click
        lstNumbers.Items.Clear()
    End Sub
End Class
