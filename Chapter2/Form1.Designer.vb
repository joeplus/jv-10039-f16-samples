﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnHighlanderExit = New System.Windows.Forms.Button()
        Me.lblDirections = New System.Windows.Forms.Label()
        Me.btnDirections = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnFive = New System.Windows.Forms.Button()
        Me.btnFour = New System.Windows.Forms.Button()
        Me.btnThree = New System.Windows.Forms.Button()
        Me.btnTwo = New System.Windows.Forms.Button()
        Me.btnOne = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnAnswer = New System.Windows.Forms.Button()
        Me.lblQuestion = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.lblStar6 = New System.Windows.Forms.Label()
        Me.lblStar3 = New System.Windows.Forms.Label()
        Me.lblStar4 = New System.Windows.Forms.Label()
        Me.lblStar2 = New System.Windows.Forms.Label()
        Me.lblStar5 = New System.Windows.Forms.Label()
        Me.lblStar7 = New System.Windows.Forms.Label()
        Me.lblStar1 = New System.Windows.Forms.Label()
        Me.btnOrionExit = New System.Windows.Forms.Button()
        Me.btnHide = New System.Windows.Forms.Button()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnNumberExit = New System.Windows.Forms.Button()
        Me.pbFive = New System.Windows.Forms.PictureBox()
        Me.pbFour = New System.Windows.Forms.PictureBox()
        Me.pbThree = New System.Windows.Forms.PictureBox()
        Me.pbTwo = New System.Windows.Forms.PictureBox()
        Me.pbOne = New System.Windows.Forms.PictureBox()
        Me.btnFrenchExit = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.pbFive, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbFour, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbThree, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbTwo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbOne, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(13, 13)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1139, 716)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnHighlanderExit)
        Me.TabPage1.Controls.Add(Me.lblDirections)
        Me.TabPage1.Controls.Add(Me.btnDirections)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.PictureBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1131, 690)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Highlander"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnHighlanderExit
        '
        Me.btnHighlanderExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHighlanderExit.Location = New System.Drawing.Point(355, 409)
        Me.btnHighlanderExit.Name = "btnHighlanderExit"
        Me.btnHighlanderExit.Size = New System.Drawing.Size(145, 58)
        Me.btnHighlanderExit.TabIndex = 4
        Me.btnHighlanderExit.Text = "Exit"
        Me.btnHighlanderExit.UseVisualStyleBackColor = True
        '
        'lblDirections
        '
        Me.lblDirections.AutoSize = True
        Me.lblDirections.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDirections.Location = New System.Drawing.Point(136, 366)
        Me.lblDirections.Name = "lblDirections"
        Me.lblDirections.Size = New System.Drawing.Size(431, 24)
        Me.lblDirections.TabIndex = 3
        Me.lblDirections.Text = "These are the directions to the Highlander Hotel ..."
        Me.lblDirections.Visible = False
        '
        'btnDirections
        '
        Me.btnDirections.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDirections.Location = New System.Drawing.Point(140, 409)
        Me.btnDirections.Name = "btnDirections"
        Me.btnDirections.Size = New System.Drawing.Size(145, 58)
        Me.btnDirections.TabIndex = 2
        Me.btnDirections.Text = "Show Directions"
        Me.btnDirections.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(184, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(291, 24)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Directions to the Highlander Hotel"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(140, 89)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(360, 262)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.btnFrenchExit)
        Me.TabPage5.Controls.Add(Me.Label2)
        Me.TabPage5.Controls.Add(Me.btnFive)
        Me.TabPage5.Controls.Add(Me.btnFour)
        Me.TabPage5.Controls.Add(Me.btnThree)
        Me.TabPage5.Controls.Add(Me.btnTwo)
        Me.TabPage5.Controls.Add(Me.btnOne)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1131, 690)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "OA - French"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(305, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(392, 24)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Click any button to see the French Translation"
        '
        'btnFive
        '
        Me.btnFive.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFive.Location = New System.Drawing.Point(661, 102)
        Me.btnFive.Name = "btnFive"
        Me.btnFive.Size = New System.Drawing.Size(75, 30)
        Me.btnFive.TabIndex = 4
        Me.btnFive.Text = "5"
        Me.btnFive.UseVisualStyleBackColor = True
        '
        'btnFour
        '
        Me.btnFour.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFour.Location = New System.Drawing.Point(547, 102)
        Me.btnFour.Name = "btnFour"
        Me.btnFour.Size = New System.Drawing.Size(75, 30)
        Me.btnFour.TabIndex = 3
        Me.btnFour.Text = "4"
        Me.btnFour.UseVisualStyleBackColor = True
        '
        'btnThree
        '
        Me.btnThree.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnThree.Location = New System.Drawing.Point(433, 102)
        Me.btnThree.Name = "btnThree"
        Me.btnThree.Size = New System.Drawing.Size(75, 30)
        Me.btnThree.TabIndex = 2
        Me.btnThree.Text = "3"
        Me.btnThree.UseVisualStyleBackColor = True
        '
        'btnTwo
        '
        Me.btnTwo.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTwo.Location = New System.Drawing.Point(319, 102)
        Me.btnTwo.Name = "btnTwo"
        Me.btnTwo.Size = New System.Drawing.Size(75, 30)
        Me.btnTwo.TabIndex = 1
        Me.btnTwo.Text = "2"
        Me.btnTwo.UseVisualStyleBackColor = True
        '
        'btnOne
        '
        Me.btnOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOne.Location = New System.Drawing.Point(205, 102)
        Me.btnOne.Name = "btnOne"
        Me.btnOne.Size = New System.Drawing.Size(75, 30)
        Me.btnOne.TabIndex = 0
        Me.btnOne.Text = "1"
        Me.btnOne.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnExit)
        Me.TabPage2.Controls.Add(Me.btnAnswer)
        Me.TabPage2.Controls.Add(Me.lblQuestion)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1131, 690)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "PC2 - Math"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(219, 80)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(113, 54)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAnswer
        '
        Me.btnAnswer.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnswer.Location = New System.Drawing.Point(37, 80)
        Me.btnAnswer.Name = "btnAnswer"
        Me.btnAnswer.Size = New System.Drawing.Size(144, 54)
        Me.btnAnswer.TabIndex = 1
        Me.btnAnswer.Text = "Show Answer"
        Me.btnAnswer.UseVisualStyleBackColor = True
        '
        'lblQuestion
        '
        Me.lblQuestion.AutoSize = True
        Me.lblQuestion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuestion.Location = New System.Drawing.Point(33, 39)
        Me.lblQuestion.Name = "lblQuestion"
        Me.lblQuestion.Size = New System.Drawing.Size(102, 24)
        Me.lblQuestion.TabIndex = 0
        Me.lblQuestion.Text = "10 + 10 = ?"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.lblStar6)
        Me.TabPage3.Controls.Add(Me.lblStar3)
        Me.TabPage3.Controls.Add(Me.lblStar4)
        Me.TabPage3.Controls.Add(Me.lblStar2)
        Me.TabPage3.Controls.Add(Me.lblStar5)
        Me.TabPage3.Controls.Add(Me.lblStar7)
        Me.TabPage3.Controls.Add(Me.lblStar1)
        Me.TabPage3.Controls.Add(Me.btnOrionExit)
        Me.TabPage3.Controls.Add(Me.btnHide)
        Me.TabPage3.Controls.Add(Me.btnShow)
        Me.TabPage3.Controls.Add(Me.PictureBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1131, 690)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "PC4 - Orion"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'lblStar6
        '
        Me.lblStar6.AutoSize = True
        Me.lblStar6.Location = New System.Drawing.Point(590, 417)
        Me.lblStar6.Name = "lblStar6"
        Me.lblStar6.Size = New System.Drawing.Size(31, 13)
        Me.lblStar6.TabIndex = 10
        Me.lblStar6.Text = "Rigel"
        Me.lblStar6.Visible = False
        '
        'lblStar3
        '
        Me.lblStar3.AutoSize = True
        Me.lblStar3.Location = New System.Drawing.Point(398, 447)
        Me.lblStar3.Name = "lblStar3"
        Me.lblStar3.Size = New System.Drawing.Size(34, 13)
        Me.lblStar3.TabIndex = 9
        Me.lblStar3.Text = "Saiph"
        Me.lblStar3.Visible = False
        '
        'lblStar4
        '
        Me.lblStar4.AutoSize = True
        Me.lblStar4.Location = New System.Drawing.Point(499, 293)
        Me.lblStar4.Name = "lblStar4"
        Me.lblStar4.Size = New System.Drawing.Size(40, 13)
        Me.lblStar4.TabIndex = 8
        Me.lblStar4.Text = "Alnilam"
        Me.lblStar4.Visible = False
        '
        'lblStar2
        '
        Me.lblStar2.AutoSize = True
        Me.lblStar2.Location = New System.Drawing.Point(395, 293)
        Me.lblStar2.Name = "lblStar2"
        Me.lblStar2.Size = New System.Drawing.Size(39, 13)
        Me.lblStar2.TabIndex = 7
        Me.lblStar2.Text = "Alnitak"
        Me.lblStar2.Visible = False
        '
        'lblStar5
        '
        Me.lblStar5.AutoSize = True
        Me.lblStar5.Location = New System.Drawing.Point(567, 255)
        Me.lblStar5.Name = "lblStar5"
        Me.lblStar5.Size = New System.Drawing.Size(45, 13)
        Me.lblStar5.TabIndex = 6
        Me.lblStar5.Text = "Mintaka"
        Me.lblStar5.Visible = False
        '
        'lblStar7
        '
        Me.lblStar7.AutoSize = True
        Me.lblStar7.Location = New System.Drawing.Point(623, 130)
        Me.lblStar7.Name = "lblStar7"
        Me.lblStar7.Size = New System.Drawing.Size(40, 13)
        Me.lblStar7.TabIndex = 5
        Me.lblStar7.Text = "Meissa"
        Me.lblStar7.Visible = False
        '
        'lblStar1
        '
        Me.lblStar1.AutoSize = True
        Me.lblStar1.Location = New System.Drawing.Point(374, 85)
        Me.lblStar1.Name = "lblStar1"
        Me.lblStar1.Size = New System.Drawing.Size(60, 13)
        Me.lblStar1.TabIndex = 4
        Me.lblStar1.Text = "Betelgeuse"
        Me.lblStar1.Visible = False
        '
        'btnOrionExit
        '
        Me.btnOrionExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOrionExit.Location = New System.Drawing.Point(644, 562)
        Me.btnOrionExit.Name = "btnOrionExit"
        Me.btnOrionExit.Size = New System.Drawing.Size(128, 62)
        Me.btnOrionExit.TabIndex = 3
        Me.btnOrionExit.Text = "Exit"
        Me.btnOrionExit.UseVisualStyleBackColor = True
        '
        'btnHide
        '
        Me.btnHide.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnHide.Location = New System.Drawing.Point(489, 562)
        Me.btnHide.Name = "btnHide"
        Me.btnHide.Size = New System.Drawing.Size(128, 62)
        Me.btnHide.TabIndex = 2
        Me.btnHide.Text = "Hide Star Names"
        Me.btnHide.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShow.Location = New System.Drawing.Point(328, 562)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(128, 62)
        Me.btnShow.TabIndex = 1
        Me.btnShow.Text = "Show Star Names"
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.Location = New System.Drawing.Point(328, 56)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(400, 470)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnNumberExit)
        Me.TabPage4.Controls.Add(Me.pbFive)
        Me.TabPage4.Controls.Add(Me.pbFour)
        Me.TabPage4.Controls.Add(Me.pbThree)
        Me.TabPage4.Controls.Add(Me.pbTwo)
        Me.TabPage4.Controls.Add(Me.pbOne)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1131, 690)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "PC7 - Numbers"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnNumberExit
        '
        Me.btnNumberExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNumberExit.Location = New System.Drawing.Point(386, 458)
        Me.btnNumberExit.Name = "btnNumberExit"
        Me.btnNumberExit.Size = New System.Drawing.Size(112, 45)
        Me.btnNumberExit.TabIndex = 5
        Me.btnNumberExit.Text = "Exit"
        Me.btnNumberExit.UseVisualStyleBackColor = True
        '
        'pbFive
        '
        Me.pbFive.Location = New System.Drawing.Point(667, 239)
        Me.pbFive.Name = "pbFive"
        Me.pbFive.Size = New System.Drawing.Size(80, 144)
        Me.pbFive.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbFive.TabIndex = 4
        Me.pbFive.TabStop = False
        '
        'pbFour
        '
        Me.pbFour.Location = New System.Drawing.Point(536, 239)
        Me.pbFour.Name = "pbFour"
        Me.pbFour.Size = New System.Drawing.Size(80, 144)
        Me.pbFour.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbFour.TabIndex = 3
        Me.pbFour.TabStop = False
        '
        'pbThree
        '
        Me.pbThree.Location = New System.Drawing.Point(398, 239)
        Me.pbThree.Name = "pbThree"
        Me.pbThree.Size = New System.Drawing.Size(80, 144)
        Me.pbThree.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbThree.TabIndex = 2
        Me.pbThree.TabStop = False
        '
        'pbTwo
        '
        Me.pbTwo.Location = New System.Drawing.Point(244, 239)
        Me.pbTwo.Name = "pbTwo"
        Me.pbTwo.Size = New System.Drawing.Size(80, 144)
        Me.pbTwo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbTwo.TabIndex = 1
        Me.pbTwo.TabStop = False
        '
        'pbOne
        '
        Me.pbOne.Location = New System.Drawing.Point(90, 239)
        Me.pbOne.Name = "pbOne"
        Me.pbOne.Size = New System.Drawing.Size(80, 144)
        Me.pbOne.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbOne.TabIndex = 0
        Me.pbOne.TabStop = False
        '
        'btnFrenchExit
        '
        Me.btnFrenchExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFrenchExit.Location = New System.Drawing.Point(433, 184)
        Me.btnFrenchExit.Name = "btnFrenchExit"
        Me.btnFrenchExit.Size = New System.Drawing.Size(75, 30)
        Me.btnFrenchExit.TabIndex = 6
        Me.btnFrenchExit.Text = "Exit"
        Me.btnFrenchExit.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1164, 741)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "COMP10039 - JV F16 Chapter 2 Samples"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.pbFive, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbFour, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbThree, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbTwo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbOne, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents btnDirections As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnExit As Button
    Friend WithEvents btnAnswer As Button
    Friend WithEvents lblQuestion As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents lblStar6 As Label
    Friend WithEvents lblStar3 As Label
    Friend WithEvents lblStar4 As Label
    Friend WithEvents lblStar2 As Label
    Friend WithEvents lblStar5 As Label
    Friend WithEvents lblStar7 As Label
    Friend WithEvents lblStar1 As Label
    Friend WithEvents btnOrionExit As Button
    Friend WithEvents btnHide As Button
    Friend WithEvents btnShow As Button
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents pbFive As PictureBox
    Friend WithEvents pbFour As PictureBox
    Friend WithEvents pbThree As PictureBox
    Friend WithEvents pbTwo As PictureBox
    Friend WithEvents pbOne As PictureBox
    Friend WithEvents btnNumberExit As Button
    Friend WithEvents btnHighlanderExit As Button
    Friend WithEvents lblDirections As Label
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents Label2 As Label
    Friend WithEvents btnFive As Button
    Friend WithEvents btnFour As Button
    Friend WithEvents btnThree As Button
    Friend WithEvents btnTwo As Button
    Friend WithEvents btnOne As Button
    Friend WithEvents btnFrenchExit As Button
End Class
