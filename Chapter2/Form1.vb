﻿Public Class Form1
    Private Sub btnAnswer_Click(sender As Object, e As EventArgs) Handles btnAnswer.Click
        lblQuestion.Text = "10 + 10 = 20"
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        lblStar1.Visible = True
        lblStar2.Visible = True
        lblStar3.Visible = True
        lblStar4.Visible = True
        lblStar5.Visible = True
        lblStar6.Visible = True
        lblStar7.Visible = True
    End Sub

    Private Sub btnHide_Click(sender As Object, e As EventArgs) Handles btnHide.Click
        lblStar1.Visible = False
        lblStar2.Visible = False
        lblStar3.Visible = False
        lblStar4.Visible = False
        lblStar5.Visible = False
        lblStar6.Visible = False
        lblStar7.Visible = False
    End Sub

    Private Sub btnOrionExit_Click(sender As Object, e As EventArgs) Handles btnOrionExit.Click
        Me.Close()
    End Sub

    Private Sub pbOne_Click(sender As Object, e As EventArgs) Handles pbOne.Click
        MessageBox.Show("You clicked the number 1", "COMP10039 - Chapter 2 - PC7")
    End Sub

    Private Sub pbTwo_Click(sender As Object, e As EventArgs) Handles pbTwo.Click
        MessageBox.Show("You clicked the number 2", "COMP10039 - Chapter 2 - PC7")
    End Sub

    Private Sub pbThree_Click(sender As Object, e As EventArgs) Handles pbThree.Click
        MessageBox.Show("You clicked the number 3", "COMP10039 - Chapter 2 - PC7")
    End Sub

    Private Sub pbFour_Click(sender As Object, e As EventArgs) Handles pbFour.Click
        MessageBox.Show("You clicked the number 4", "COMP10039 - Chapter 2 - PC7")
    End Sub

    Private Sub pbFive_Click(sender As Object, e As EventArgs) Handles pbFive.Click
        MessageBox.Show("You clicked the number 5", "COMP10039 - Chapter 2 - PC7")
    End Sub

    Private Sub btnNumberExit_Click(sender As Object, e As EventArgs) Handles btnNumberExit.Click
        Me.Close()
    End Sub

    Private Sub btnDirections_Click(sender As Object, e As EventArgs) Handles btnDirections.Click
        lblDirections.Visible = True
    End Sub

    Private Sub btnHighlanderExit_Click(sender As Object, e As EventArgs) Handles btnHighlanderExit.Click
        Me.Close()
    End Sub

    Private Sub btnOne_Click(sender As Object, e As EventArgs) Handles btnOne.Click
        MessageBox.Show("Un", "COMP10039 Ch2 OA")
    End Sub

    Private Sub btnTwo_Click(sender As Object, e As EventArgs) Handles btnTwo.Click
        MessageBox.Show("Deux", "COMP10039 Ch2 OA")
    End Sub

    Private Sub btnThree_Click(sender As Object, e As EventArgs) Handles btnThree.Click
        MessageBox.Show("Trois", "COMP10039 Ch2 OA")
    End Sub

    Private Sub btnFour_Click(sender As Object, e As EventArgs) Handles btnFour.Click
        MessageBox.Show("Quatre", "COMP10039 Ch2 OA")
    End Sub

    Private Sub btnFive_Click(sender As Object, e As EventArgs) Handles btnFive.Click
        MessageBox.Show("Cinq", "COMP10039 Ch2 OA")
    End Sub

    Private Sub btnFrenchExit_Click(sender As Object, e As EventArgs) Handles btnFrenchExit.Click
        Me.Close()
    End Sub
End Class
