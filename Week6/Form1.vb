﻿Public Class Form1
    Private Const decTAX_RATE As Decimal = 0.13

    Private Sub btnToggleLight_Click(sender As Object, e As EventArgs) Handles btnToggleLight.Click
        If (picOff.Visible) Then
            SetLightState(True)
        Else
            SetLightState(False)
        End If
    End Sub

    Private Sub TurnLightOn()
        picOff.Visible = False
        picOn.Visible = True
        lblLightState.Text = "Light ON"
    End Sub

    Private Sub TurnLightOff()
        picOff.Visible = True
        picOn.Visible = False
        lblLightState.Text = "Light OFF"
    End Sub

    Private Sub SetLightState(blnState As Boolean)
        If blnState = True Then
            TurnLightOn()
        Else
            TurnLightOff()
        End If
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Dim decSalary As Decimal = nudSalary.Value

        CalculateNewSalaryByValue(decSalary, nudIncrease.Value)
        lblSalaryByVal.Text = decSalary.ToString("C")

        CalculateNewSalaryByReference(decSalary, nudIncrease.Value)
        lblSalaryByRef.Text = decSalary.ToString("C")

        'decSalary = CalculateNewSalaryByValueUsingFunction(nudSalary.Value, nudIncrease.Value)
        lblFunctionValue.Text = CalculateNewSalaryByValueUsingFunction(nudSalary.Value,
                                                                       nudIncrease.Value).ToString("C")
    End Sub

    Private Sub CalculateNewSalaryByValue(decSalary As Decimal, decIncrease As Decimal)
        decSalary *= (1 + decIncrease / 100)
    End Sub

    Private Sub CalculateNewSalaryByReference(ByRef decSalary As Decimal, decIncrease As Decimal)
        decSalary *= (1 + decIncrease / 100)
    End Sub

    Private Function CalculateNewSalaryByValueUsingFunction(decSalary As Decimal, decIncrease As Decimal) As Decimal
        decSalary *= (1 + decIncrease / 100)
        Return decSalary
    End Function

    Private Sub btnBagelCalculate_Click(sender As Object, e As EventArgs) Handles btnBagelCalculate.Click
        Dim decSubTotal As Decimal

        decSubTotal = BagelCost() + ToppingCost() + CoffeeCost()
        lblSubtotal.Text = decSubTotal.ToString("C")

        Dim decTax As Decimal = CalcTax(decSubTotal)
        lblTax.Text = decTax.ToString("C")

        lblTotal.Text = (decSubTotal + decTax).ToString("C")
    End Sub

    Private Function BagelCost() As Decimal
        If (radWhite.Checked) Then
            Return 1.25
        Else
            Return 1.5
        End If
    End Function

    Private Function ToppingCost() As Decimal
        Dim decToppings As Decimal

        If (chkButter.Checked) Then
            decToppings += 0.25
        End If

        If (chkCream.Checked) Then
            decToppings += 0.5
        End If

        If (chkBlue.Checked) Then
            decToppings += 0.75
        End If

        If (chkRasp.Checked) Then
            decToppings += 0.75
        End If
        If (chkPeach.Checked) Then
            decToppings += 0.75
        End If

        Return decToppings
    End Function

    Private Function CoffeeCost() As Decimal
        If radNone.Checked Then
            Return 0
        ElseIf radCoffee.Checked Then
            Return 1.25
        ElseIf radCap.Checked Then
            Return 2.0
        ElseIf radLatte.Checked Then
            Return 1.75
        Else
            Return 0
        End If
    End Function

    Private Function CalcTax(Subtotal As Decimal) As Decimal
        Return Subtotal * decTAX_RATE
    End Function

    Private Sub ResetBagels()
        radWheat.Checked = True
    End Sub

    Private Sub ResetToppings()
        chkBlue.Checked = False
        chkButter.Checked = False
        chkCream.Checked = False
        chkPeach.Checked = False
        chkRasp.Checked = False
    End Sub

    Private Sub ResetCoffee()
        radNone.Checked = True
    End Sub

    Private Sub ResetPrice()
        lblSubtotal.Text = String.Empty
        lblTax.Text = String.Empty
        lblTotal.Text = String.Empty
    End Sub

    Private Sub btnBagelReset_Click(sender As Object, e As EventArgs) Handles btnBagelReset.Click
        ResetBagels()
        ResetCoffee()
        ResetToppings()
        ResetPrice()
    End Sub

    Private Sub pbRock_Click(sender As Object, e As EventArgs) Handles pbRock.Click
        Dim intComputer As Integer = ComputerGuess()
        GameOutcome(0, intComputer)
    End Sub

    Private Sub pbPaper_Click(sender As Object, e As EventArgs) Handles pbPaper.Click
        Dim intComputer As Integer = ComputerGuess()
        GameOutcome(1, intComputer)
    End Sub

    Private Sub pbScissors_Click(sender As Object, e As EventArgs) Handles pbScissors.Click
        Dim intComputer As Integer = ComputerGuess()
        GameOutcome(2, intComputer)
    End Sub

    Private Function ComputerGuess() As Integer
        Dim intGuess As Integer = CInt(Math.Floor(Rnd() * 3))

        If (intGuess = 0) Then
            pbComputer.Image = My.Resources.Rock
        ElseIf (intGuess = 1) Then
            pbComputer.Image = My.Resources.Paper
        ElseIf (intGuess = 2) Then
            pbComputer.Image = My.Resources.Scissors
        End If
        Return intGuess
    End Function

    Private Sub GameOutcome(intUserGuess As Integer, intComputerGuess As Integer)
        If intUserGuess = intComputerGuess Then
            lblOutcome.Text = "DRAW!"
            sstResult.BackColor = Color.Yellow
        ElseIf (intUserGuess = 0 And intComputerGuess = 2) Or
                (intUserGuess = 1 And intComputerGuess = 0) Or
                (intUserGuess = 2 And intComputerGuess = 1) Then
            lblOutcome.Text = "YOU WIN!"
            sstResult.BackColor = Color.Green
        Else
            lblOutcome.Text = "YOU LOST!"
            sstResult.BackColor = Color.Red
        End If
    End Sub
End Class
