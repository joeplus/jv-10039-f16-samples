﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnToggleLight = New System.Windows.Forms.Button()
        Me.lblLightState = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lblFunctionValue = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.lblSalaryByRef = New System.Windows.Forms.Label()
        Me.lblSalaryByVal = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nudIncrease = New System.Windows.Forms.NumericUpDown()
        Me.nudSalary = New System.Windows.Forms.NumericUpDown()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnBagelExit = New System.Windows.Forms.Button()
        Me.btnBagelReset = New System.Windows.Forms.Button()
        Me.btnBagelCalculate = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.lblSubtotal = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.radLatte = New System.Windows.Forms.RadioButton()
        Me.radCap = New System.Windows.Forms.RadioButton()
        Me.radCoffee = New System.Windows.Forms.RadioButton()
        Me.radNone = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkPeach = New System.Windows.Forms.CheckBox()
        Me.chkBlue = New System.Windows.Forms.CheckBox()
        Me.chkRasp = New System.Windows.Forms.CheckBox()
        Me.chkButter = New System.Windows.Forms.CheckBox()
        Me.chkCream = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.radWheat = New System.Windows.Forms.RadioButton()
        Me.radWhite = New System.Windows.Forms.RadioButton()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.picOn = New System.Windows.Forms.PictureBox()
        Me.picOff = New System.Windows.Forms.PictureBox()
        Me.pbScissors = New System.Windows.Forms.PictureBox()
        Me.pbPaper = New System.Windows.Forms.PictureBox()
        Me.pbRock = New System.Windows.Forms.PictureBox()
        Me.pbComputer = New System.Windows.Forms.PictureBox()
        Me.sstResult = New System.Windows.Forms.StatusStrip()
        Me.lblOutcome = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.nudIncrease, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudSalary, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.picOn, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picOff, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbScissors, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbPaper, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbRock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbComputer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.sstResult.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1265, 814)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnToggleLight)
        Me.TabPage1.Controls.Add(Me.lblLightState)
        Me.TabPage1.Controls.Add(Me.picOn)
        Me.TabPage1.Controls.Add(Me.picOff)
        Me.TabPage1.Location = New System.Drawing.Point(4, 33)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1257, 777)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnToggleLight
        '
        Me.btnToggleLight.Location = New System.Drawing.Point(285, 609)
        Me.btnToggleLight.Name = "btnToggleLight"
        Me.btnToggleLight.Size = New System.Drawing.Size(159, 50)
        Me.btnToggleLight.TabIndex = 3
        Me.btnToggleLight.Text = "Toggle Light"
        Me.btnToggleLight.UseVisualStyleBackColor = True
        '
        'lblLightState
        '
        Me.lblLightState.AutoSize = True
        Me.lblLightState.Location = New System.Drawing.Point(487, 549)
        Me.lblLightState.Name = "lblLightState"
        Me.lblLightState.Size = New System.Drawing.Size(94, 24)
        Me.lblLightState.TabIndex = 2
        Me.lblLightState.Text = "Light OFF"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.lblFunctionValue)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.btnCalculate)
        Me.TabPage2.Controls.Add(Me.lblSalaryByRef)
        Me.TabPage2.Controls.Add(Me.lblSalaryByVal)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.nudIncrease)
        Me.TabPage2.Controls.Add(Me.nudSalary)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 33)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1257, 777)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'lblFunctionValue
        '
        Me.lblFunctionValue.AutoSize = True
        Me.lblFunctionValue.Location = New System.Drawing.Point(306, 245)
        Me.lblFunctionValue.Name = "lblFunctionValue"
        Me.lblFunctionValue.Size = New System.Drawing.Size(0, 24)
        Me.lblFunctionValue.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(11, 245)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(281, 24)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "New Salary (By Function Value):"
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(203, 284)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(103, 45)
        Me.btnCalculate.TabIndex = 8
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'lblSalaryByRef
        '
        Me.lblSalaryByRef.AutoSize = True
        Me.lblSalaryByRef.Location = New System.Drawing.Point(306, 206)
        Me.lblSalaryByRef.Name = "lblSalaryByRef"
        Me.lblSalaryByRef.Size = New System.Drawing.Size(0, 24)
        Me.lblSalaryByRef.TabIndex = 7
        '
        'lblSalaryByVal
        '
        Me.lblSalaryByVal.AutoSize = True
        Me.lblSalaryByVal.Location = New System.Drawing.Point(306, 158)
        Me.lblSalaryByVal.Name = "lblSalaryByVal"
        Me.lblSalaryByVal.Size = New System.Drawing.Size(0, 24)
        Me.lblSalaryByVal.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(51, 206)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(241, 24)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "New Salary (By Reference):"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(90, 158)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(202, 24)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "New Salary (By Value):"
        '
        'nudIncrease
        '
        Me.nudIncrease.Location = New System.Drawing.Point(306, 101)
        Me.nudIncrease.Name = "nudIncrease"
        Me.nudIncrease.Size = New System.Drawing.Size(120, 29)
        Me.nudIncrease.TabIndex = 3
        '
        'nudSalary
        '
        Me.nudSalary.Location = New System.Drawing.Point(306, 53)
        Me.nudSalary.Name = "nudSalary"
        Me.nudSalary.Size = New System.Drawing.Size(120, 29)
        Me.nudSalary.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(173, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 24)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Increase (%):"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(226, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Salary:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnBagelExit)
        Me.TabPage3.Controls.Add(Me.btnBagelReset)
        Me.TabPage3.Controls.Add(Me.btnBagelCalculate)
        Me.TabPage3.Controls.Add(Me.GroupBox4)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Controls.Add(Me.GroupBox1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 33)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1257, 777)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Bagel Shop"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btnBagelExit
        '
        Me.btnBagelExit.Location = New System.Drawing.Point(413, 480)
        Me.btnBagelExit.Name = "btnBagelExit"
        Me.btnBagelExit.Size = New System.Drawing.Size(120, 50)
        Me.btnBagelExit.TabIndex = 6
        Me.btnBagelExit.Text = "Exit"
        Me.btnBagelExit.UseVisualStyleBackColor = True
        '
        'btnBagelReset
        '
        Me.btnBagelReset.Location = New System.Drawing.Point(244, 480)
        Me.btnBagelReset.Name = "btnBagelReset"
        Me.btnBagelReset.Size = New System.Drawing.Size(120, 50)
        Me.btnBagelReset.TabIndex = 5
        Me.btnBagelReset.Text = "Reset"
        Me.btnBagelReset.UseVisualStyleBackColor = True
        '
        'btnBagelCalculate
        '
        Me.btnBagelCalculate.Location = New System.Drawing.Point(69, 480)
        Me.btnBagelCalculate.Name = "btnBagelCalculate"
        Me.btnBagelCalculate.Size = New System.Drawing.Size(120, 50)
        Me.btnBagelCalculate.TabIndex = 4
        Me.btnBagelCalculate.Text = "Calculate"
        Me.btnBagelCalculate.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.lblTotal)
        Me.GroupBox4.Controls.Add(Me.lblTax)
        Me.GroupBox4.Controls.Add(Me.lblSubtotal)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Location = New System.Drawing.Point(302, 206)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(293, 231)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Price"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(104, 140)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(0, 24)
        Me.lblTotal.TabIndex = 5
        '
        'lblTax
        '
        Me.lblTax.AutoSize = True
        Me.lblTax.Location = New System.Drawing.Point(104, 91)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(0, 24)
        Me.lblTax.TabIndex = 4
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(104, 40)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(0, 24)
        Me.lblSubtotal.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(35, 140)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 24)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Total:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(44, 90)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 24)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Tax:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 40)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 24)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Subtotal:"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radLatte)
        Me.GroupBox3.Controls.Add(Me.radCap)
        Me.GroupBox3.Controls.Add(Me.radCoffee)
        Me.GroupBox3.Controls.Add(Me.radNone)
        Me.GroupBox3.Location = New System.Drawing.Point(302, 25)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(293, 175)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Want Coffee with That?"
        '
        'radLatte
        '
        Me.radLatte.AutoSize = True
        Me.radLatte.Location = New System.Drawing.Point(16, 134)
        Me.radLatte.Name = "radLatte"
        Me.radLatte.Size = New System.Drawing.Size(129, 28)
        Me.radLatte.TabIndex = 3
        Me.radLatte.Text = "Latte ($1.75)"
        Me.radLatte.UseVisualStyleBackColor = True
        '
        'radCap
        '
        Me.radCap.AutoSize = True
        Me.radCap.Location = New System.Drawing.Point(16, 99)
        Me.radCap.Name = "radCap"
        Me.radCap.Size = New System.Drawing.Size(192, 28)
        Me.radCap.TabIndex = 2
        Me.radCap.Text = "Cappuccino ($2.00)"
        Me.radCap.UseVisualStyleBackColor = True
        '
        'radCoffee
        '
        Me.radCoffee.AutoSize = True
        Me.radCoffee.Location = New System.Drawing.Point(16, 64)
        Me.radCoffee.Name = "radCoffee"
        Me.radCoffee.Size = New System.Drawing.Size(215, 28)
        Me.radCoffee.TabIndex = 1
        Me.radCoffee.Text = "Regular Coffee ($1.25)"
        Me.radCoffee.UseVisualStyleBackColor = True
        '
        'radNone
        '
        Me.radNone.AutoSize = True
        Me.radNone.Checked = True
        Me.radNone.Location = New System.Drawing.Point(16, 29)
        Me.radNone.Name = "radNone"
        Me.radNone.Size = New System.Drawing.Size(75, 28)
        Me.radNone.TabIndex = 0
        Me.radNone.TabStop = True
        Me.radNone.Text = "None"
        Me.radNone.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkPeach)
        Me.GroupBox2.Controls.Add(Me.chkBlue)
        Me.GroupBox2.Controls.Add(Me.chkRasp)
        Me.GroupBox2.Controls.Add(Me.chkButter)
        Me.GroupBox2.Controls.Add(Me.chkCream)
        Me.GroupBox2.Location = New System.Drawing.Point(33, 206)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(253, 231)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Pick Toppings"
        '
        'chkPeach
        '
        Me.chkPeach.AutoSize = True
        Me.chkPeach.Location = New System.Drawing.Point(18, 171)
        Me.chkPeach.Name = "chkPeach"
        Me.chkPeach.Size = New System.Drawing.Size(177, 28)
        Me.chkPeach.TabIndex = 4
        Me.chkPeach.Text = "Peach Jelly ($.75)"
        Me.chkPeach.UseVisualStyleBackColor = True
        '
        'chkBlue
        '
        Me.chkBlue.AutoSize = True
        Me.chkBlue.Location = New System.Drawing.Point(18, 105)
        Me.chkBlue.Name = "chkBlue"
        Me.chkBlue.Size = New System.Drawing.Size(202, 28)
        Me.chkBlue.TabIndex = 3
        Me.chkBlue.Text = "Blueberry Jam ($.75)"
        Me.chkBlue.UseVisualStyleBackColor = True
        '
        'chkRasp
        '
        Me.chkRasp.AutoSize = True
        Me.chkRasp.Location = New System.Drawing.Point(18, 138)
        Me.chkRasp.Name = "chkRasp"
        Me.chkRasp.Size = New System.Drawing.Size(207, 28)
        Me.chkRasp.TabIndex = 2
        Me.chkRasp.Text = "Raspberry Jam ($.75)"
        Me.chkRasp.UseVisualStyleBackColor = True
        '
        'chkButter
        '
        Me.chkButter.AutoSize = True
        Me.chkButter.Location = New System.Drawing.Point(18, 72)
        Me.chkButter.Name = "chkButter"
        Me.chkButter.Size = New System.Drawing.Size(129, 28)
        Me.chkButter.TabIndex = 1
        Me.chkButter.Text = "Butter ($.25)"
        Me.chkButter.UseVisualStyleBackColor = True
        '
        'chkCream
        '
        Me.chkCream.AutoSize = True
        Me.chkCream.Location = New System.Drawing.Point(18, 39)
        Me.chkCream.Name = "chkCream"
        Me.chkCream.Size = New System.Drawing.Size(208, 28)
        Me.chkCream.TabIndex = 0
        Me.chkCream.Text = "Cream Cheese ($.50)"
        Me.chkCream.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radWheat)
        Me.GroupBox1.Controls.Add(Me.radWhite)
        Me.GroupBox1.Location = New System.Drawing.Point(33, 25)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(253, 100)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pick a Bagel"
        '
        'radWheat
        '
        Me.radWheat.AutoSize = True
        Me.radWheat.Checked = True
        Me.radWheat.Location = New System.Drawing.Point(18, 64)
        Me.radWheat.Name = "radWheat"
        Me.radWheat.Size = New System.Drawing.Size(204, 28)
        Me.radWheat.TabIndex = 1
        Me.radWheat.TabStop = True
        Me.radWheat.Text = "Whole Wheat ($1.50)"
        Me.radWheat.UseVisualStyleBackColor = True
        '
        'radWhite
        '
        Me.radWhite.AutoSize = True
        Me.radWhite.Location = New System.Drawing.Point(18, 29)
        Me.radWhite.Name = "radWhite"
        Me.radWhite.Size = New System.Drawing.Size(138, 28)
        Me.radWhite.TabIndex = 0
        Me.radWhite.Text = "White ($1.25)"
        Me.radWhite.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.pbComputer)
        Me.TabPage4.Controls.Add(Me.pbScissors)
        Me.TabPage4.Controls.Add(Me.pbPaper)
        Me.TabPage4.Controls.Add(Me.pbRock)
        Me.TabPage4.Location = New System.Drawing.Point(4, 33)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1257, 777)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "TabPage4"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'picOn
        '
        Me.picOn.Image = CType(resources.GetObject("picOn.Image"), System.Drawing.Image)
        Me.picOn.Location = New System.Drawing.Point(558, 75)
        Me.picOn.Name = "picOn"
        Me.picOn.Size = New System.Drawing.Size(329, 440)
        Me.picOn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picOn.TabIndex = 1
        Me.picOn.TabStop = False
        Me.picOn.Visible = False
        '
        'picOff
        '
        Me.picOff.Image = CType(resources.GetObject("picOff.Image"), System.Drawing.Image)
        Me.picOff.Location = New System.Drawing.Point(144, 75)
        Me.picOff.Name = "picOff"
        Me.picOff.Size = New System.Drawing.Size(329, 440)
        Me.picOff.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.picOff.TabIndex = 0
        Me.picOff.TabStop = False
        '
        'pbScissors
        '
        Me.pbScissors.Image = Global.Week6.My.Resources.Resources.Scissors
        Me.pbScissors.Location = New System.Drawing.Point(286, 51)
        Me.pbScissors.Name = "pbScissors"
        Me.pbScissors.Size = New System.Drawing.Size(64, 64)
        Me.pbScissors.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbScissors.TabIndex = 2
        Me.pbScissors.TabStop = False
        '
        'pbPaper
        '
        Me.pbPaper.Image = Global.Week6.My.Resources.Resources.Paper
        Me.pbPaper.Location = New System.Drawing.Point(167, 51)
        Me.pbPaper.Name = "pbPaper"
        Me.pbPaper.Size = New System.Drawing.Size(64, 64)
        Me.pbPaper.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbPaper.TabIndex = 1
        Me.pbPaper.TabStop = False
        '
        'pbRock
        '
        Me.pbRock.Image = Global.Week6.My.Resources.Resources.Rock
        Me.pbRock.Location = New System.Drawing.Point(49, 51)
        Me.pbRock.Name = "pbRock"
        Me.pbRock.Size = New System.Drawing.Size(64, 64)
        Me.pbRock.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbRock.TabIndex = 0
        Me.pbRock.TabStop = False
        '
        'pbComputer
        '
        Me.pbComputer.Location = New System.Drawing.Point(454, 51)
        Me.pbComputer.Name = "pbComputer"
        Me.pbComputer.Size = New System.Drawing.Size(100, 50)
        Me.pbComputer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.pbComputer.TabIndex = 3
        Me.pbComputer.TabStop = False
        '
        'sstResult
        '
        Me.sstResult.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblOutcome})
        Me.sstResult.Location = New System.Drawing.Point(0, 866)
        Me.sstResult.Name = "sstResult"
        Me.sstResult.Size = New System.Drawing.Size(1289, 22)
        Me.sstResult.TabIndex = 1
        Me.sstResult.Text = "StatusStrip1"
        '
        'lblOutcome
        '
        Me.lblOutcome.Name = "lblOutcome"
        Me.lblOutcome.Size = New System.Drawing.Size(70, 17)
        Me.lblOutcome.Text = "lblOutcome"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 24.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1289, 888)
        Me.Controls.Add(Me.sstResult)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(6)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.nudIncrease, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudSalary, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.picOn, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picOff, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbScissors, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbPaper, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbRock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbComputer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.sstResult.ResumeLayout(False)
        Me.sstResult.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnToggleLight As Button
    Friend WithEvents lblLightState As Label
    Friend WithEvents picOn As PictureBox
    Friend WithEvents picOff As PictureBox
    Friend WithEvents btnCalculate As Button
    Friend WithEvents lblSalaryByRef As Label
    Friend WithEvents lblSalaryByVal As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents nudIncrease As NumericUpDown
    Friend WithEvents nudSalary As NumericUpDown
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblFunctionValue As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents btnBagelExit As Button
    Friend WithEvents btnBagelReset As Button
    Friend WithEvents btnBagelCalculate As Button
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents lblTotal As Label
    Friend WithEvents lblTax As Label
    Friend WithEvents lblSubtotal As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents radLatte As RadioButton
    Friend WithEvents radCap As RadioButton
    Friend WithEvents radCoffee As RadioButton
    Friend WithEvents radNone As RadioButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents chkPeach As CheckBox
    Friend WithEvents chkBlue As CheckBox
    Friend WithEvents chkRasp As CheckBox
    Friend WithEvents chkButter As CheckBox
    Friend WithEvents chkCream As CheckBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents radWheat As RadioButton
    Friend WithEvents radWhite As RadioButton
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents pbPaper As PictureBox
    Friend WithEvents pbRock As PictureBox
    Friend WithEvents pbScissors As PictureBox
    Friend WithEvents pbComputer As PictureBox
    Friend WithEvents sstResult As StatusStrip
    Friend WithEvents lblOutcome As ToolStripStatusLabel
End Class
