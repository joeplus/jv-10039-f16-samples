﻿Public Class Form1
    ' Class level variables
    Dim classDateString As String
    Const SALES_TAX As Decimal = 0.13
    Const DEFAULT_STRING As String = "Please enter the date fields above"
    Const COUNTY_TAX_RATE As Decimal = 0.02
    Const STATE_TAX_RATE As Decimal = 0.04


    Private Sub btnShow_Click(sender As Object, e As EventArgs) Handles btnShow.Click
        'Local variable - can be seen in the method only
        Dim localDateString = txtDay.Text + " " + txtMonth.Text + " " + txtDate.Text + ", " + txtYear.Text
        classDateString = localDateString
        lblDateString.Text = classDateString
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtDate.Clear()
        txtDay.Clear()
        txtMonth.Clear()
        txtYear.Clear()
        lblDateString.Text = DEFAULT_STRING
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblDate.Text = Now.ToString("D")
        lblTime.Text = Now.ToString("T")
    End Sub

    Private Sub btnHotelExit_Click(sender As Object, e As EventArgs) Handles btnHotelExit.Click
        Close()
    End Sub

    Private Sub btnHotelClear_Click(sender As Object, e As EventArgs) Handles btnHotelClear.Click
        lblRoomCharges.Text = String.Empty
        lblAdditional.Text = String.Empty
        lblSubtotal.Text = String.Empty
        lblTax.Text = String.Empty
        lblTotal.Text = String.Empty

        txtRoomService.Text = String.Empty
        txtTelephone.Text = String.Empty
        txtMisc.Text = String.Empty

        nudNights.Value = 1
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As EventArgs) Handles btnCalculate.Click
        Try
            Dim decRoomCharges As Decimal = nudNights.Value * CDec(txtRate.Text)
            lblRoomCharges.Text = decRoomCharges.ToString("C")

            Dim decAdditionalCharges As Decimal = CDec(txtRoomService.Text) + CDec(txtTelephone.Text) + CDec(txtMisc.Text)
            lblAdditional.Text = decAdditionalCharges.ToString("C")

            Dim decSubtotal As Decimal = decRoomCharges + decAdditionalCharges
            lblSubtotal.Text = decSubtotal.ToString("C")

            Dim decTax As Decimal = decSubtotal * SALES_TAX
            lblTax.Text = decTax.ToString("C")

            Dim decTotal As Decimal = decSubtotal + decTax
            lblTotal.Text = decTotal.ToString("C")

        Catch Ex As Exception
            MessageBox.Show("One of the fields is not a valid number. Exception: " + Ex.Message, "COMP10039 Hotel")
        End Try
    End Sub

    Private Sub btnMpg_Click(sender As Object, e As EventArgs) Handles btnMpg.Click
        lblMpg.Text = (nudMiles.Value / nudGallons.Value).ToString("G")
    End Sub

    Private Sub btnClearMpg_Click(sender As Object, e As EventArgs) Handles btnClearMpg.Click
        lblMpg.Text = String.Empty
        nudGallons.Value = nudGallons.Minimum
        nudMiles.Value = nudMiles.Minimum
    End Sub

    Private Sub btnMpgExit_Click(sender As Object, e As EventArgs) Handles btnMpgExit.Click
        Close()
    End Sub

    Private Sub Label23_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnSeatingRevenue_Click(sender As Object, e As EventArgs) Handles btnSeatingRevenue.Click
        Dim classARevenue As Decimal = nudClassA.Value * 15
        Dim classBRevenue As Decimal = nudClassB.Value * 12
        Dim classCRevenue As Decimal = nudClassC.Value * 9
        Dim totalRevenue As Decimal = classARevenue + classBRevenue + classCRevenue

        lblClassA.Text = classARevenue.ToString("C")
        lblClassB.Text = classBRevenue.ToString("C")
        lblClassC.Text = classCRevenue.ToString("C")
        lblRevenue.Text = totalRevenue.ToString("C")

    End Sub

    Private Sub btnSeatingClear_Click(sender As Object, e As EventArgs) Handles btnSeatingClear.Click
        nudClassA.Value = 0
        nudClassB.Value = 0
        nudClassC.Value = 0
        lblClassA.Text = String.Empty
        lblClassB.Text = String.Empty
        lblClassC.Text = String.Empty
        lblRevenue.Text = String.Empty
    End Sub

    Private Sub btnSeatingExit_Click(sender As Object, e As EventArgs) Handles btnSeatingExit.Click
        Close()
    End Sub

    Private Sub nudSales_ValueChanged(sender As Object, e As EventArgs) Handles nudSales.ValueChanged
    End Sub

    Private Sub btnTax_Click(sender As Object, e As EventArgs) Handles btnTax.Click
        lblCountyTax.Text = (nudSales.Value * COUNTY_TAX_RATE).ToString("C")
        lblStateTax.Text = (nudSales.Value * STATE_TAX_RATE).ToString("C")
        lblTotalTax.Text = ((nudSales.Value * COUNTY_TAX_RATE) + (nudSales.Value * STATE_TAX_RATE)).ToString("C")
    End Sub

    Private Sub btnTaxExit_Click(sender As Object, e As EventArgs) Handles btnTaxExit.Click
        Close()
    End Sub

    Private Sub btnBMI_Click(sender As Object, e As EventArgs) Handles btnBMI.Click
        Dim dblBMI As Double = nudWeight.Value * 703 / nudHeight.Value ^ 2
        lblBMI.Text = dblBMI.ToString("G")
    End Sub

    Private Sub btnBMIExit_Click(sender As Object, e As EventArgs) Handles btnBMIExit.Click
        Close()
    End Sub
End Class
