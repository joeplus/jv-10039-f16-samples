﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnShow = New System.Windows.Forms.Button()
        Me.lblDateString = New System.Windows.Forms.Label()
        Me.txtYear = New System.Windows.Forms.TextBox()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.txtMonth = New System.Windows.Forms.TextBox()
        Me.txtDay = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnHotelExit = New System.Windows.Forms.Button()
        Me.btnHotelClear = New System.Windows.Forms.Button()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.lblTax = New System.Windows.Forms.Label()
        Me.lblSubtotal = New System.Windows.Forms.Label()
        Me.lblAdditional = New System.Windows.Forms.Label()
        Me.lblRoomCharges = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtMisc = New System.Windows.Forms.TextBox()
        Me.txtTelephone = New System.Windows.Forms.TextBox()
        Me.txtRoomService = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.lblDate = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtRate = New System.Windows.Forms.TextBox()
        Me.nudNights = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.btnMpgExit = New System.Windows.Forms.Button()
        Me.btnClearMpg = New System.Windows.Forms.Button()
        Me.btnMpg = New System.Windows.Forms.Button()
        Me.lblMpg = New System.Windows.Forms.Label()
        Me.nudMiles = New System.Windows.Forms.NumericUpDown()
        Me.nudGallons = New System.Windows.Forms.NumericUpDown()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnSeatingExit = New System.Windows.Forms.Button()
        Me.btnSeatingClear = New System.Windows.Forms.Button()
        Me.btnSeatingRevenue = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lblRevenue = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.lblClassC = New System.Windows.Forms.Label()
        Me.lblClassB = New System.Windows.Forms.Label()
        Me.lblClassA = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.nudClassC = New System.Windows.Forms.NumericUpDown()
        Me.nudClassB = New System.Windows.Forms.NumericUpDown()
        Me.nudClassA = New System.Windows.Forms.NumericUpDown()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.btnTaxExit = New System.Windows.Forms.Button()
        Me.btnTax = New System.Windows.Forms.Button()
        Me.lblTotalTax = New System.Windows.Forms.Label()
        Me.lblStateTax = New System.Windows.Forms.Label()
        Me.lblCountyTax = New System.Windows.Forms.Label()
        Me.nudSales = New System.Windows.Forms.NumericUpDown()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.btnBMIExit = New System.Windows.Forms.Button()
        Me.btnBMI = New System.Windows.Forms.Button()
        Me.lblBMI = New System.Windows.Forms.Label()
        Me.nudHeight = New System.Windows.Forms.NumericUpDown()
        Me.nudWeight = New System.Windows.Forms.NumericUpDown()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.nudNights, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.nudMiles, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudGallons, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.nudClassC, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudClassB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudClassA, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        CType(Me.nudSales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.nudHeight, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudWeight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1140, 718)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.btnExit)
        Me.TabPage1.Controls.Add(Me.btnClear)
        Me.TabPage1.Controls.Add(Me.btnShow)
        Me.TabPage1.Controls.Add(Me.lblDateString)
        Me.TabPage1.Controls.Add(Me.txtYear)
        Me.TabPage1.Controls.Add(Me.txtDate)
        Me.TabPage1.Controls.Add(Me.txtMonth)
        Me.TabPage1.Controls.Add(Me.txtDay)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 33)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Date String"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExit.Location = New System.Drawing.Point(326, 236)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(115, 64)
        Me.btnExit.TabIndex = 11
        Me.btnExit.Text = "E&xit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnClear
        '
        Me.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClear.Location = New System.Drawing.Point(181, 236)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(115, 64)
        Me.btnClear.TabIndex = 10
        Me.btnClear.Text = "&Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnShow
        '
        Me.btnShow.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShow.Location = New System.Drawing.Point(31, 236)
        Me.btnShow.Name = "btnShow"
        Me.btnShow.Size = New System.Drawing.Size(115, 64)
        Me.btnShow.TabIndex = 9
        Me.btnShow.Text = "&Show Date"
        Me.btnShow.UseVisualStyleBackColor = True
        '
        'lblDateString
        '
        Me.lblDateString.AutoSize = True
        Me.lblDateString.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateString.Location = New System.Drawing.Point(27, 188)
        Me.lblDateString.Name = "lblDateString"
        Me.lblDateString.Size = New System.Drawing.Size(0, 24)
        Me.lblDateString.TabIndex = 8
        '
        'txtYear
        '
        Me.txtYear.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYear.Location = New System.Drawing.Point(196, 145)
        Me.txtYear.Name = "txtYear"
        Me.txtYear.Size = New System.Drawing.Size(100, 26)
        Me.txtYear.TabIndex = 7
        '
        'txtDate
        '
        Me.txtDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDate.Location = New System.Drawing.Point(196, 112)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(100, 26)
        Me.txtDate.TabIndex = 6
        '
        'txtMonth
        '
        Me.txtMonth.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMonth.Location = New System.Drawing.Point(196, 79)
        Me.txtMonth.Name = "txtMonth"
        Me.txtMonth.Size = New System.Drawing.Size(100, 26)
        Me.txtMonth.TabIndex = 5
        '
        'txtDay
        '
        Me.txtDay.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDay.Location = New System.Drawing.Point(196, 46)
        Me.txtDay.Name = "txtDay"
        Me.txtDay.Size = New System.Drawing.Size(100, 26)
        Me.txtDay.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(135, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 24)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Year:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(27, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(162, 24)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Date of the Month:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(121, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(68, 24)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Month:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(37, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(152, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Day of the Week:"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnHotelExit)
        Me.TabPage2.Controls.Add(Me.btnHotelClear)
        Me.TabPage2.Controls.Add(Me.btnCalculate)
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.GroupBox2)
        Me.TabPage2.Controls.Add(Me.lblTime)
        Me.TabPage2.Controls.Add(Me.lblDate)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 33)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Hotel"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnHotelExit
        '
        Me.btnHotelExit.Location = New System.Drawing.Point(371, 599)
        Me.btnHotelExit.Name = "btnHotelExit"
        Me.btnHotelExit.Size = New System.Drawing.Size(100, 39)
        Me.btnHotelExit.TabIndex = 9
        Me.btnHotelExit.Text = "Exit"
        Me.btnHotelExit.UseVisualStyleBackColor = True
        '
        'btnHotelClear
        '
        Me.btnHotelClear.Location = New System.Drawing.Point(222, 599)
        Me.btnHotelClear.Name = "btnHotelClear"
        Me.btnHotelClear.Size = New System.Drawing.Size(100, 39)
        Me.btnHotelClear.TabIndex = 8
        Me.btnHotelClear.Text = "Clear"
        Me.btnHotelClear.UseVisualStyleBackColor = True
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(70, 599)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(100, 39)
        Me.btnCalculate.TabIndex = 7
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblTotal)
        Me.GroupBox3.Controls.Add(Me.lblTax)
        Me.GroupBox3.Controls.Add(Me.lblSubtotal)
        Me.GroupBox3.Controls.Add(Me.lblAdditional)
        Me.GroupBox3.Controls.Add(Me.lblRoomCharges)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Location = New System.Drawing.Point(50, 311)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(518, 255)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Total Charges"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(209, 201)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(0, 24)
        Me.lblTotal.TabIndex = 9
        '
        'lblTax
        '
        Me.lblTax.AutoSize = True
        Me.lblTax.Location = New System.Drawing.Point(209, 162)
        Me.lblTax.Name = "lblTax"
        Me.lblTax.Size = New System.Drawing.Size(0, 24)
        Me.lblTax.TabIndex = 8
        '
        'lblSubtotal
        '
        Me.lblSubtotal.AutoSize = True
        Me.lblSubtotal.Location = New System.Drawing.Point(209, 123)
        Me.lblSubtotal.Name = "lblSubtotal"
        Me.lblSubtotal.Size = New System.Drawing.Size(0, 24)
        Me.lblSubtotal.TabIndex = 7
        '
        'lblAdditional
        '
        Me.lblAdditional.AutoSize = True
        Me.lblAdditional.Location = New System.Drawing.Point(209, 84)
        Me.lblAdditional.Name = "lblAdditional"
        Me.lblAdditional.Size = New System.Drawing.Size(0, 24)
        Me.lblAdditional.TabIndex = 6
        '
        'lblRoomCharges
        '
        Me.lblRoomCharges.AutoSize = True
        Me.lblRoomCharges.Location = New System.Drawing.Point(209, 45)
        Me.lblRoomCharges.Name = "lblRoomCharges"
        Me.lblRoomCharges.Size = New System.Drawing.Size(0, 24)
        Me.lblRoomCharges.TabIndex = 5
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(48, 201)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(132, 24)
        Me.Label16.TabIndex = 4
        Me.Label16.Text = "Total Charges:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(133, 162)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(47, 24)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Tax:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(98, 123)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(82, 24)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Subtotal:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(174, 24)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "Additional Charges:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(38, 45)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(142, 24)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Room Charges:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtMisc)
        Me.GroupBox2.Controls.Add(Me.txtTelephone)
        Me.GroupBox2.Controls.Add(Me.txtRoomService)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Location = New System.Drawing.Point(301, 121)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(267, 157)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Additional Charges"
        '
        'txtMisc
        '
        Me.txtMisc.Location = New System.Drawing.Point(148, 103)
        Me.txtMisc.Name = "txtMisc"
        Me.txtMisc.Size = New System.Drawing.Size(100, 29)
        Me.txtMisc.TabIndex = 5
        '
        'txtTelephone
        '
        Me.txtTelephone.Location = New System.Drawing.Point(148, 68)
        Me.txtTelephone.Name = "txtTelephone"
        Me.txtTelephone.Size = New System.Drawing.Size(100, 29)
        Me.txtTelephone.TabIndex = 4
        '
        'txtRoomService
        '
        Me.txtRoomService.Location = New System.Drawing.Point(148, 33)
        Me.txtRoomService.Name = "txtRoomService"
        Me.txtRoomService.Size = New System.Drawing.Size(100, 29)
        Me.txtRoomService.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(88, 106)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(54, 24)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "Misc:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(34, 71)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(108, 24)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "Telephone:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(8, 36)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(134, 24)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Room Service:"
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(368, 76)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(0, 24)
        Me.lblTime.TabIndex = 4
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Location = New System.Drawing.Point(368, 29)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(0, 24)
        Me.lblDate.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(304, 76)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(58, 24)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Time:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(309, 29)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 24)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Date:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtRate)
        Me.GroupBox1.Controls.Add(Me.nudNights)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(50, 121)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(244, 157)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Room Information"
        '
        'txtRate
        '
        Me.txtRate.Location = New System.Drawing.Point(102, 76)
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Size = New System.Drawing.Size(120, 29)
        Me.txtRate.TabIndex = 3
        Me.txtRate.Text = "999.99"
        '
        'nudNights
        '
        Me.nudNights.Location = New System.Drawing.Point(102, 32)
        Me.nudNights.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudNights.Name = "nudNights"
        Me.nudNights.Size = New System.Drawing.Size(120, 29)
        Me.nudNights.TabIndex = 2
        Me.nudNights.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(31, 79)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 24)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Rate:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 34)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 24)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Nights:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.btnMpgExit)
        Me.TabPage3.Controls.Add(Me.btnClearMpg)
        Me.TabPage3.Controls.Add(Me.btnMpg)
        Me.TabPage3.Controls.Add(Me.lblMpg)
        Me.TabPage3.Controls.Add(Me.nudMiles)
        Me.TabPage3.Controls.Add(Me.nudGallons)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.Label18)
        Me.TabPage3.Controls.Add(Me.Label17)
        Me.TabPage3.Location = New System.Drawing.Point(4, 33)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "PC1 - MPG"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'btnMpgExit
        '
        Me.btnMpgExit.Location = New System.Drawing.Point(268, 200)
        Me.btnMpgExit.Name = "btnMpgExit"
        Me.btnMpgExit.Size = New System.Drawing.Size(107, 58)
        Me.btnMpgExit.TabIndex = 8
        Me.btnMpgExit.Text = "Exit"
        Me.btnMpgExit.UseVisualStyleBackColor = True
        '
        'btnClearMpg
        '
        Me.btnClearMpg.Location = New System.Drawing.Point(141, 200)
        Me.btnClearMpg.Name = "btnClearMpg"
        Me.btnClearMpg.Size = New System.Drawing.Size(107, 58)
        Me.btnClearMpg.TabIndex = 7
        Me.btnClearMpg.Text = "Clear"
        Me.btnClearMpg.UseVisualStyleBackColor = True
        '
        'btnMpg
        '
        Me.btnMpg.Location = New System.Drawing.Point(7, 200)
        Me.btnMpg.Name = "btnMpg"
        Me.btnMpg.Size = New System.Drawing.Size(107, 58)
        Me.btnMpg.TabIndex = 6
        Me.btnMpg.Text = "Calculate MPG"
        Me.btnMpg.UseVisualStyleBackColor = True
        '
        'lblMpg
        '
        Me.lblMpg.AutoSize = True
        Me.lblMpg.Location = New System.Drawing.Point(181, 136)
        Me.lblMpg.Name = "lblMpg"
        Me.lblMpg.Size = New System.Drawing.Size(0, 24)
        Me.lblMpg.TabIndex = 5
        '
        'nudMiles
        '
        Me.nudMiles.Location = New System.Drawing.Point(181, 86)
        Me.nudMiles.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudMiles.Minimum = New Decimal(New Integer() {100, 0, 0, 0})
        Me.nudMiles.Name = "nudMiles"
        Me.nudMiles.Size = New System.Drawing.Size(120, 29)
        Me.nudMiles.TabIndex = 4
        Me.nudMiles.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'nudGallons
        '
        Me.nudGallons.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudGallons.Location = New System.Drawing.Point(181, 39)
        Me.nudGallons.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudGallons.Name = "nudGallons"
        Me.nudGallons.Size = New System.Drawing.Size(120, 29)
        Me.nudGallons.TabIndex = 3
        Me.nudGallons.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(27, 136)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(148, 24)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "Miles per gallon:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(16, 88)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(159, 24)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Miles per full tank:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 41)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(172, 24)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Gallons of gas tank:"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnSeatingExit)
        Me.TabPage4.Controls.Add(Me.btnSeatingClear)
        Me.TabPage4.Controls.Add(Me.btnSeatingRevenue)
        Me.TabPage4.Controls.Add(Me.GroupBox5)
        Me.TabPage4.Controls.Add(Me.GroupBox4)
        Me.TabPage4.Location = New System.Drawing.Point(4, 33)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "PC2 - Seating"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnSeatingExit
        '
        Me.btnSeatingExit.Location = New System.Drawing.Point(477, 302)
        Me.btnSeatingExit.Name = "btnSeatingExit"
        Me.btnSeatingExit.Size = New System.Drawing.Size(195, 59)
        Me.btnSeatingExit.TabIndex = 4
        Me.btnSeatingExit.Text = "Exit"
        Me.btnSeatingExit.UseVisualStyleBackColor = True
        '
        'btnSeatingClear
        '
        Me.btnSeatingClear.Location = New System.Drawing.Point(241, 302)
        Me.btnSeatingClear.Name = "btnSeatingClear"
        Me.btnSeatingClear.Size = New System.Drawing.Size(195, 59)
        Me.btnSeatingClear.TabIndex = 3
        Me.btnSeatingClear.Text = "Clear"
        Me.btnSeatingClear.UseVisualStyleBackColor = True
        '
        'btnSeatingRevenue
        '
        Me.btnSeatingRevenue.Location = New System.Drawing.Point(6, 302)
        Me.btnSeatingRevenue.Name = "btnSeatingRevenue"
        Me.btnSeatingRevenue.Size = New System.Drawing.Size(195, 59)
        Me.btnSeatingRevenue.TabIndex = 2
        Me.btnSeatingRevenue.Text = "Calculate Revenue"
        Me.btnSeatingRevenue.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lblRevenue)
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.lblClassC)
        Me.GroupBox5.Controls.Add(Me.lblClassB)
        Me.GroupBox5.Controls.Add(Me.lblClassA)
        Me.GroupBox5.Controls.Add(Me.Label23)
        Me.GroupBox5.Controls.Add(Me.Label24)
        Me.GroupBox5.Controls.Add(Me.Label25)
        Me.GroupBox5.Location = New System.Drawing.Point(343, 67)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(329, 207)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Revenue Generated"
        '
        'lblRevenue
        '
        Me.lblRevenue.AutoSize = True
        Me.lblRevenue.Location = New System.Drawing.Point(144, 150)
        Me.lblRevenue.Name = "lblRevenue"
        Me.lblRevenue.Size = New System.Drawing.Size(0, 24)
        Me.lblRevenue.TabIndex = 10
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(10, 150)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(138, 24)
        Me.Label26.TabIndex = 9
        Me.Label26.Text = "Total Revenue:"
        '
        'lblClassC
        '
        Me.lblClassC.AutoSize = True
        Me.lblClassC.Location = New System.Drawing.Point(153, 109)
        Me.lblClassC.Name = "lblClassC"
        Me.lblClassC.Size = New System.Drawing.Size(0, 24)
        Me.lblClassC.TabIndex = 8
        '
        'lblClassB
        '
        Me.lblClassB.AutoSize = True
        Me.lblClassB.Location = New System.Drawing.Point(152, 71)
        Me.lblClassB.Name = "lblClassB"
        Me.lblClassB.Size = New System.Drawing.Size(0, 24)
        Me.lblClassB.TabIndex = 7
        '
        'lblClassA
        '
        Me.lblClassA.AutoSize = True
        Me.lblClassA.Location = New System.Drawing.Point(149, 33)
        Me.lblClassA.Name = "lblClassA"
        Me.lblClassA.Size = New System.Drawing.Size(0, 24)
        Me.lblClassA.TabIndex = 6
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(70, 109)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(78, 24)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Class C:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(71, 71)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(77, 24)
        Me.Label24.TabIndex = 4
        Me.Label24.Text = "Class B:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(70, 33)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(78, 24)
        Me.Label25.TabIndex = 3
        Me.Label25.Text = "Class A:"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.nudClassC)
        Me.GroupBox4.Controls.Add(Me.nudClassB)
        Me.GroupBox4.Controls.Add(Me.nudClassA)
        Me.GroupBox4.Controls.Add(Me.Label22)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Location = New System.Drawing.Point(6, 67)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(331, 207)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Tickets Sold"
        '
        'nudClassC
        '
        Me.nudClassC.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudClassC.Location = New System.Drawing.Point(106, 112)
        Me.nudClassC.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudClassC.Name = "nudClassC"
        Me.nudClassC.Size = New System.Drawing.Size(120, 29)
        Me.nudClassC.TabIndex = 5
        '
        'nudClassB
        '
        Me.nudClassB.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudClassB.Location = New System.Drawing.Point(106, 74)
        Me.nudClassB.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudClassB.Name = "nudClassB"
        Me.nudClassB.Size = New System.Drawing.Size(120, 29)
        Me.nudClassB.TabIndex = 4
        '
        'nudClassA
        '
        Me.nudClassA.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nudClassA.Location = New System.Drawing.Point(106, 36)
        Me.nudClassA.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.nudClassA.Name = "nudClassA"
        Me.nudClassA.Size = New System.Drawing.Size(120, 29)
        Me.nudClassA.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(22, 114)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(78, 24)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Class C:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(22, 76)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(77, 24)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Class B:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(22, 38)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(78, 24)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Class A:"
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.btnTaxExit)
        Me.TabPage5.Controls.Add(Me.btnTax)
        Me.TabPage5.Controls.Add(Me.lblTotalTax)
        Me.TabPage5.Controls.Add(Me.lblStateTax)
        Me.TabPage5.Controls.Add(Me.lblCountyTax)
        Me.TabPage5.Controls.Add(Me.nudSales)
        Me.TabPage5.Controls.Add(Me.Label30)
        Me.TabPage5.Controls.Add(Me.Label29)
        Me.TabPage5.Controls.Add(Me.Label28)
        Me.TabPage5.Controls.Add(Me.Label27)
        Me.TabPage5.Location = New System.Drawing.Point(4, 33)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "PC10 - Tax"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'btnTaxExit
        '
        Me.btnTaxExit.Location = New System.Drawing.Point(192, 225)
        Me.btnTaxExit.Name = "btnTaxExit"
        Me.btnTaxExit.Size = New System.Drawing.Size(133, 47)
        Me.btnTaxExit.TabIndex = 9
        Me.btnTaxExit.Text = "Exit"
        Me.btnTaxExit.UseVisualStyleBackColor = True
        '
        'btnTax
        '
        Me.btnTax.Location = New System.Drawing.Point(19, 225)
        Me.btnTax.Name = "btnTax"
        Me.btnTax.Size = New System.Drawing.Size(133, 47)
        Me.btnTax.TabIndex = 8
        Me.btnTax.Text = "Calculate Tax"
        Me.btnTax.UseVisualStyleBackColor = True
        '
        'lblTotalTax
        '
        Me.lblTotalTax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblTotalTax.Location = New System.Drawing.Point(156, 175)
        Me.lblTotalTax.Name = "lblTotalTax"
        Me.lblTotalTax.Size = New System.Drawing.Size(118, 26)
        Me.lblTotalTax.TabIndex = 7
        '
        'lblStateTax
        '
        Me.lblStateTax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblStateTax.Location = New System.Drawing.Point(156, 127)
        Me.lblStateTax.Name = "lblStateTax"
        Me.lblStateTax.Size = New System.Drawing.Size(118, 26)
        Me.lblStateTax.TabIndex = 6
        '
        'lblCountyTax
        '
        Me.lblCountyTax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblCountyTax.Location = New System.Drawing.Point(156, 79)
        Me.lblCountyTax.Name = "lblCountyTax"
        Me.lblCountyTax.Size = New System.Drawing.Size(118, 26)
        Me.lblCountyTax.TabIndex = 5
        '
        'nudSales
        '
        Me.nudSales.Location = New System.Drawing.Point(156, 24)
        Me.nudSales.Name = "nudSales"
        Me.nudSales.Size = New System.Drawing.Size(120, 29)
        Me.nudSales.TabIndex = 4
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(57, 175)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(93, 24)
        Me.Label30.TabIndex = 3
        Me.Label30.Text = "Total Tax:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(57, 127)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(93, 24)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "State Tax:"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(39, 79)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(111, 24)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "County Tax:"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(18, 26)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(132, 24)
        Me.Label27.TabIndex = 0
        Me.Label27.Text = "Monthly Sales:"
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.btnBMIExit)
        Me.TabPage6.Controls.Add(Me.btnBMI)
        Me.TabPage6.Controls.Add(Me.lblBMI)
        Me.TabPage6.Controls.Add(Me.nudHeight)
        Me.TabPage6.Controls.Add(Me.nudWeight)
        Me.TabPage6.Controls.Add(Me.Label33)
        Me.TabPage6.Controls.Add(Me.Label32)
        Me.TabPage6.Controls.Add(Me.Label31)
        Me.TabPage6.Location = New System.Drawing.Point(4, 33)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(1132, 681)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "PC14 - BMI"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'btnBMIExit
        '
        Me.btnBMIExit.Location = New System.Drawing.Point(291, 179)
        Me.btnBMIExit.Name = "btnBMIExit"
        Me.btnBMIExit.Size = New System.Drawing.Size(165, 56)
        Me.btnBMIExit.TabIndex = 7
        Me.btnBMIExit.Text = "Exit"
        Me.btnBMIExit.UseVisualStyleBackColor = True
        '
        'btnBMI
        '
        Me.btnBMI.Location = New System.Drawing.Point(64, 179)
        Me.btnBMI.Name = "btnBMI"
        Me.btnBMI.Size = New System.Drawing.Size(165, 56)
        Me.btnBMI.TabIndex = 6
        Me.btnBMI.Text = "Calculate BMI"
        Me.btnBMI.UseVisualStyleBackColor = True
        '
        'lblBMI
        '
        Me.lblBMI.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblBMI.Location = New System.Drawing.Point(164, 111)
        Me.lblBMI.Name = "lblBMI"
        Me.lblBMI.Size = New System.Drawing.Size(120, 24)
        Me.lblBMI.TabIndex = 5
        '
        'nudHeight
        '
        Me.nudHeight.Location = New System.Drawing.Point(164, 69)
        Me.nudHeight.Maximum = New Decimal(New Integer() {120, 0, 0, 0})
        Me.nudHeight.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudHeight.Name = "nudHeight"
        Me.nudHeight.Size = New System.Drawing.Size(120, 29)
        Me.nudHeight.TabIndex = 4
        Me.nudHeight.Value = New Decimal(New Integer() {66, 0, 0, 0})
        '
        'nudWeight
        '
        Me.nudWeight.Location = New System.Drawing.Point(164, 24)
        Me.nudWeight.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.nudWeight.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudWeight.Name = "nudWeight"
        Me.nudWeight.Size = New System.Drawing.Size(120, 29)
        Me.nudWeight.TabIndex = 3
        Me.nudWeight.Value = New Decimal(New Integer() {140, 0, 0, 0})
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(111, 111)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(47, 24)
        Me.Label33.TabIndex = 2
        Me.Label33.Text = "BMI:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(15, 71)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(143, 24)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "Height (inches):"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(3, 26)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(155, 24)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Weight (pounds):"
        '
        'Form1
        '
        Me.AcceptButton = Me.btnShow
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnClear
        Me.ClientSize = New System.Drawing.Size(1164, 742)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "COMP10039 - JV F16 Chapter 3 Samples"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.nudNights, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.nudMiles, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudGallons, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.nudClassC, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudClassB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudClassA, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.nudSales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.nudHeight, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudWeight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents btnExit As Button
    Friend WithEvents btnClear As Button
    Friend WithEvents btnShow As Button
    Friend WithEvents lblDateString As Label
    Friend WithEvents txtYear As TextBox
    Friend WithEvents txtDate As TextBox
    Friend WithEvents txtMonth As TextBox
    Friend WithEvents txtDay As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblTime As Label
    Friend WithEvents lblDate As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtRate As TextBox
    Friend WithEvents nudNights As NumericUpDown
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents btnHotelExit As Button
    Friend WithEvents btnHotelClear As Button
    Friend WithEvents btnCalculate As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents lblTotal As Label
    Friend WithEvents lblTax As Label
    Friend WithEvents lblSubtotal As Label
    Friend WithEvents lblAdditional As Label
    Friend WithEvents lblRoomCharges As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents txtMisc As TextBox
    Friend WithEvents txtTelephone As TextBox
    Friend WithEvents txtRoomService As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents btnMpgExit As Button
    Friend WithEvents btnClearMpg As Button
    Friend WithEvents btnMpg As Button
    Friend WithEvents lblMpg As Label
    Friend WithEvents nudMiles As NumericUpDown
    Friend WithEvents nudGallons As NumericUpDown
    Friend WithEvents Label19 As Label
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents nudClassC As NumericUpDown
    Friend WithEvents nudClassB As NumericUpDown
    Friend WithEvents nudClassA As NumericUpDown
    Friend WithEvents Label22 As Label
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents btnSeatingExit As Button
    Friend WithEvents btnSeatingClear As Button
    Friend WithEvents btnSeatingRevenue As Button
    Friend WithEvents lblClassC As Label
    Friend WithEvents lblClassB As Label
    Friend WithEvents lblClassA As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents lblRevenue As Label
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents lblTotalTax As Label
    Friend WithEvents lblStateTax As Label
    Friend WithEvents lblCountyTax As Label
    Friend WithEvents nudSales As NumericUpDown
    Friend WithEvents Label30 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents btnTax As Button
    Friend WithEvents btnTaxExit As Button
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents btnBMIExit As Button
    Friend WithEvents btnBMI As Button
    Friend WithEvents lblBMI As Label
    Friend WithEvents nudHeight As NumericUpDown
    Friend WithEvents nudWeight As NumericUpDown
    Friend WithEvents Label33 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents Label31 As Label
End Class
